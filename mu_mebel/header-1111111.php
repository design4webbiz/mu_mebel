<!doctype html>
<html lang=ru>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Document</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/media.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/common.js"></script>
</head>
<body>
<!--modal login-->

<div class="modal fade login_modal" id="myModalLogin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img
                        src="img/close-modal.png" alt=""></button>
                <h4 class="modal-title" id="myModalLabel">Войти</h4>
            </div>
            <div class="modal-body">
                <form action="">
                    <div class="form-group">
                        <label for="InputEmail1">Email:</label>
                        <input type="email" class="form-control" id="leInputEmail1"
                               placeholder="Укажите Ваш Email">
                    </div>
                    <div class="form-group">
                        <label for="InputPass">Пароль:</label><a href="" class="pull-right">Напомнить пароль</a>
                        <input type="email" class="form-control" id="InputPass"
                               placeholder="Введите пароль">
                    </div>
                    <p class="text-center" style="margin-top: 40px"><a href="">Хочу зарегистрироваться</a></p>

                    <div class="form-group">
                        <input type="checkbox" id="remember"> &nbsp; <label for="remember">Запомнить меня</label>
                    </div>
                </form>
            </div>
            <button type="button" class="btn btn-primary center-block button_submit">ОК</button>
        </div>
    </div>
</div>
<!-- / modal login-->
<header>
    <div class="container">
        <div class="top_menu">
            <button type="button" class="navbar-toggle phone_menu">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <nav>

                <ul class="list-inline">
                    <li><a href="">О компании</a></li>
                    <li><a href="">Доставка и оплата</a></li>
                    <li><a href="">Услуги</a></li>
                    <li><a href="">Статьи</a></li>
                    <li><a href="">Новости</a></li>
                    <li><a href="">Контакты</a></li>
                    <li class="login"><a href="" data-toggle="modal" data-target="#myModalLogin"> <img
                                src="img/log-aut.png" alt="">
                            <span>Вход</span></a>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="row">
            <div class="col-md-6 no_padding">
                <div class="col-sm-3">
                    <img class="img-responsive logo" src="img/logo1.png" alt="">
                </div>
                <div class="col-sm-5">
                    <p class="delivery">Доставка по Москве и России</p>
                </div>
                <div class="col-sm-4 no_padding_left">
                    <div class="work_time">
                        <p><img src="img/clock.png" alt=""> 9.00-21.00</p>

                        <p class="phone_number">8 952 400 8000</p>

                        <p><a href="">Перезвонить Вам?</a></p>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="col-xs-5 col-sm-2">
                    <img src="img/img-icon.png" alt="">
                </div>
                <div class="col-xs-7 col-sm-4">
                    <p class="guarantee">
                        Гарантия 14 дней с момента доставки
                    </p>
                </div>
                <div class="clearfix visible-xs"></div>
                <div class="col-sm-6">
                    <ul class="list-inline cart_block">
                        <li><a href=""><img src="img/icon-like.png" alt="">

                                <p>Нравится</p></a></li>
                        <li><a href=""><img src="img/icon-home.png" alt="">

                                <p>Сравнение</p></a></li>
                        <li><a href=""><img src="img/icon-cart.png" alt="">

                                <p>Корзина</p></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10">
                <div class="block_menu">
                    <nav>
                        <ul class="list-inline menu_1">
                            <li><a href="">компьютерние столы</a></li>
                            <li><a href="">шкафы купе</a></li>
                            <li><a href="">диваны</a></li>
                            <li><a href="">матрасы</a></li>
                            <li><a href="">журнальные столы</a></li>
                            <li><a href="">тумбы</a></li>
                            <li><a href="">стулья</a></li>
                            <li><a href="">светильики</a></li>
                            <li><a href="">кровати</a></li>
                        </ul>
                        <ul class="list-inline menu_2">
                            <li><a href="">кухня</a>
                                <ul class="list-unstyled down_menu_list">
                                    <li><a href="">Готовые кухни</a></li>
                                    <li><a href="">Кухни помодульно</a></li>
                                    <li><a href="">Отдельные предметы</a></li>
                                    <li><a href="">Обедденные столи</a></li>
                                    <li><a href="">Стулья и табуреты</a></li>
                                    <li><a href="">Кухонные уголки</a></li>
                                    <li><a href="">Кухонные решения</a></li>
                                </ul>
                            </li>
                            <li><a href="">спальня</a>
                                <ul class="list-unstyled down_menu_list">
                                    <li><a href="">111111111111111111</a></li>
                                    <li><a href="">22222222222222</a></li>
                                    <li><a href="">О5454545454</a></li>
                                    <li><a href="">54545</a></li>
                                    <li><a href="">545454522012045</a></li>
                                    <li><a href="">4525242450450</a></li>
                                    <li><a href="">45245245274524752452</a></li>
                                </ul>
                            </li>
                            <li><a href="">гостиння</a></li>
                            <li><a href="">прихожая</a></li>
                            <li><a href="">детская</a></li>
                            <li><a href="">подростковая</a></li>
                            <li><a href="">кабинет</a></li>
                            <li><a href="">акции</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="col-md-2 no_padding_left">
                <form action="">

                    <div class="dropdown filter_search1">
                        <a class=" dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">
                            Готовые решения &nbsp;<img src="img/donw-icon.png" alt=""></a>
                        <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                            <li role="presentation"><a role="menuitem" href="#">HTML</a></li>
                            <li role="presentation"><a role="menuitem" href="#">CSS</a></li>
                            <li role="presentation"><a role="menuitem" href="#">JavaScript</a></li>
                            <li role="presentation" class="divider"></li>
                            <li role="presentation"><a role="menuitem" href="#">About Us</a></li>
                        </ul>
                    </div>


                    <div class="dropdown filter_search2">
                        <a class=" dropdown-toggle " type="button" id="menu2" data-toggle="dropdown">
                            По производителям &nbsp;<img src="img/donw-icon.png" alt=""></a>
                        <ul class="dropdown-menu" role="menu" aria-labelledby="menu2">
                            <li role="presentation"><a role="menuitem" href="#">1424524</a></li>
                            <li role="presentation"><a role="menuitem" href="#">41412452</a></li>
                            <li role="presentation"><a role="menuitem" href="#">52452</a></li>
                            <li role="presentation"><a role="menuitem" href="#">471414</a></li>
                        </ul>
                    </div>
                    <div class="block_search">
                        <button class="search" type="submit"><img src="img/icon-search.png" alt=""></button>
                        <div class="select">
                            <select class="form-control" name="">
                                <option value="">диваны</option>
                                <option value="">матрасы</option>
                                <option value="">шкафы купе</option>
                            </select>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</header>

<script>
    $(document).ready(function () {
        $('.phone_menu').on('click', function () {
            $('.top_menu ul').slideToggle();
        });
    });
</script>