<?php include_once("header.php"); ?>

<div class="container product_page">

    <div class="bread_crums">
        <ol class="breadcrumb">
            <li><a href="#">Главная</a></li>
            <li><a href="#">Библиотека</a></li>
            <li class="active">Данные</li>
        </ol>
    </div>

    <div class="title text-center">
        <span>Тумба прикроватная мягкая 2 ящика</span>
    </div>
    <div class="row">
        <div class="col-md-6 col-sm-12" >
            <div class="block_header ">

                <div class="manufacturer text-left">
                    <img src="img/manufactured.png" alt="">
                    Производитель: <a href="#">Горизонт</a>
                </div>

                <div class="question text-right">
                    <img src="img/question.png" alt="" style="height: 20px;">
                    <a href="#">Задать вопрос по этому товару</a>
                </div>
            </div>


        </div>

        <div class="col-md-6 col-sm-12  " >
            <div class="block_header row " >
                <div class=" article col-md-2 col-sm-12 no_padding">
                    Aртикул: <span>85652</span>
                </div>
                <div class=" rating text-center col-sm-12 col-md-7">
                    Рейтинг:
                    <ul class="list-inline list-unstyled">
                        <li><img src="img/stars.png" alt=""></li>
                        <li><img src="img/stars.png" alt=""></li>
                        <li><img src="img/stars.png" alt=""></li>
                        <li><img src="img/stars.png" alt=""></li>
                        <li><img src="img/stars.png" alt=""></li>
                    </ul>
                    (<span>10</span> голосов)




                </div>

                <div class=" text-right col-md-3 col-sm-12 garanty no_padding ">
                    Гарантия 12 месяцев
                </div>
            </div>

        </div>
    </div>


    <div class="col-md-6 no_padding_left">
        <div class="item_content">
            <div class="img_big">
                <img src="img/big_img.png" alt="">
            </div>

            <div class="col-md-12 mobile_padding no_padding_left">
                <div class="soc_lis_item ">
                    <span>
                        Поделится c друзями:
                    </span>
                    <ul class="list-inline list-unstyled">
                        <li><a href="href=#"><img src="img/f_vk.png" alt="vk"></a></li>
                        <li><a href="href=#"><img src="img/f_ok.png" alt="ok"></a></li>
                        <li><a href="href=#"><img src="img/f_fb.png" alt="fb"></a></li>
                        <li><a href="href=#"><img src="img/f_tw.png" alt="fb"></a></li>
                        <li><a href="href=#"><img src="img/f_gl.png" alt="google"></a></li>
                        <li><a href="href=#"><img src="img/f_sm.png" alt="sm"></a></li>
                    </ul>
                </div>

            </div>
            <div class="img_desk hidden-xs hidden-sm ">
            <div class="col-md-3 no_padding_left ">
                <img src="img/big_img.png" alt="">
            </div>
                <div class="col-md-3  no_padding_left">
                    <img src="img/big_img.png" alt="">
                </div>
                <div class="col-md-3 no_padding_left">
                    <img src="img/big_img.png" alt="">
                </div>
                <div class="col-md-3 no_padding ">
                    <img src="img/big_img.png" alt="">
                </div>
            </div>

        </div>


    </div>
    <div class="col-md-3">
        
            <ul class="list-unstyled subject hidden-xs hidden-sm">
                <li>Габариты(ШхВ хГ)..................600х400х520(мм)</li>
                <li>Матириал......................ЛДСП 16мм, ДВП 20мм</li>
                <li>Гарантия........................................................5 лет</li>
            </ul>

        <span class="text-right variants hidden-xs hidden-sm ">Варианты цветовго исполнения:</span>

        <div class="colors row hidden-xs hidden-sm">
            <div class="col-md-4  ">
                <img src="img/colors.png" alt="">
            </div>
            <div class="col-md-4  ">
                <img src="img/colors.png" alt="">
            </div>
            <div class="col-md-4  ">
                <img src="img/colors.png" alt="">
            </div>


        </div>

        <p class="old_prise no_padding col-xs-12 mob_center"> Старая цена: <span>6500p.</span></p>
        <p class="total_cost no_padding col-xs-12 mob_center"> Цена: <span>5400 p</span></p>

        <div class="quantity total  col-xs-12 mob_center">
            <button class="minus" style="font-size: 37px;" >
                -
            </button>

            <input #id="count" type="text" value="1">

            <button  class="plus">
                +
            </button>

        </div>


            <div class="buy_block mob_center">
            <button class="buy_button">
                купить
            </button>



            <img class="margin_left hidden-xs hidden-sm" src="img/icon-like.png" alt="" >
            <img  class="hidden-xs hidden-sm" src="img/icon-home.png" alt="">

                <a class="mob_center" style="display:block;" href="#">Купить в 1 клик</a>

            </div>
        


    </div>
    <div class="col-md-3 no_padding_right hidden-xs hidden-sm">
        <div class="right_side_bar">

            <img src="img/details.png" alt="">
            <span class="name_sidebar">
                Подробние условия:
            </span>

            <span class="name_list">Доставка</span>
            <ul class="list-unstyled">
                <li><span class="bold_text">Доставка по Москве</span> - безплатно</li>
                <li><span class="bold_text">Доставка по МО за МКАД</span> - 100p/км</li>
                <li><span class="bold_text">Доставка по России</span> - до</li>
                <li>транспортной компании - безплатно</li>
            </ul>

            <span class="name_list" >Ожидаемое время доставки</span>
            <ul class="list-unstyled">
                <li><span class="bold_text">Москва и область</span>- 1-2 дня</li>
                <li><span class="bold_text">По России</span>- 1-2 дней</li>
            </ul>

            <span class="name_list">Оплата </span>
            <ul class="list-unstyled">
                <li><span class="bold_text">Москва и МО</span> наличными при</li>
                <li>получении, безналичными</li>
                <li><span class="bold_text">Россия</span>- безналичный расчет</li>
            </ul>

            <span class="name_list">Гарантия 12 месяцев</span>
            <ul class="list-unstyled">
                <li><span class="bold_text">12 месяцев </span> официальной гарантии</li>
                <li>от производтеля</li>
                <li>Обмен/возврат товара в течении</li>
                <li>14 дней, при невскритой упаковке</li>
            </ul>

        </div>

    </div>

<!--tabs-->


        <div class="tabs_section col-md-12 hidden-xs ">
            <ul class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#decr" aria-controls="decr" role="tab" data-toggle="tab" style="    margin-left: 16px;">Описание</a></li>
                <li><a href="#variants" aria-controls="variants" role="tab" data-toggle="tab">Варианты серии</a></li>
                <li><a href="#dostavka" aria-controls="dostavka" role="tab" data-toggle="tab">Доставка и оплата</a></li>
                <li><a href="#garantia" aria-controls="garantia" role="tab" data-toggle="tab">Гарантия</a></li>
                <li><a href="feedback" aria-controls="feedback" role="tab" data-toggle="tab">Отзывы</a></li>
            </ul>
            <!-- Содержимое вкладок -->
            <div class="tab-content row">
                <div role="tabpanel" class="tab-pane active" id="decr">
                    <div class="col-md-9 ">
                        <div class="decription_tabs ">
                            <ul class="list-unstyled">
                                <li><span>Размеры спального места Д/Ш/В,мм:</span> 198 x 140 x 41</li>
                                <li><span>Внешние размеры Д/Ш/В,мм:</span> 198 x 140 x 41.Высота сиденья пола: 41см</li>
                                <li><span>Механизм трансвормации:</span> Аккордеон</li>
                                <li><span>Каркас:</span> Металокаркас</li>
                                <li><span>Стандартное наполнение Наполнение спального места:</span> ППУ</li>
                                <li><span>Примичание:</span> <span>Материалы изделия по фото:</span>Ткань основа: Фреш арлекино 01- микровелюр
                                (см "Наполнение и комплектация")
                                </li>
                                <li><span>Гарантия:</span> 18 месяцев</li>
                                <li><span>Производеитель:</span> Мебель холдинг</li>
                                <li><span>Строк доставки</span> от 3 дней </li>
                                <li>Материал низ и спинка дивана выполнение из массива породы дерева - бук</li>
                                <li><span>Комплектация</span> Наличее ящика для белья. Ортопедическая решетка.</li>
                                <li><span>Модель:</span>Диван из натурального дерева</li>
                            </ul>

                            <div class="mo_decr">
                                <img src="img/yellow_icon.png" alt="">
                                У нас есть еще фотографии етого товара - смотрите ниже!
                            </div>

                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="dowloand">
                            <span>Инструкция по сборкe:</span><br>
                            <img src="img/pdf.png" alt="">

                            <a href="#">Тумба прикроватная мягкая 2 ящика Горизон.pdf</a>

                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="variants">

                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium aliquam animi aut commodi dolore dolorem doloribus, ducimus earum, eius eligendi eum in ipsam iure magni maxime molestias nemo odit perferendis ratione rerum saepe tempora tenetur. Asperiores autem beatae esse non pariatur quos tempora totam! At consectetur distinctio eveniet ipsa optio perferendis? At, blanditiis commodi dolor dolorum, esse explicabo fugiat iure libero magnam nemo neque nihil, nobis quibusdam quod voluptas. Accusamus aperiam at, beatae, culpa dolor error est eveniet ex illum impedit inventore magni minus nihil nobis non nostrum numquam officia possimus quisquam reiciendis sed sunt tempora temporibus totam velit, voluptates.
                </div>
                <div role="tabpanel" class="tab-pane" id="dostavka">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad adipisci alias amet architecto at commodi consequuntur, deleniti, distinctio dolor ducimus enim est excepturi exercitationem expedita fugiat fugit illum incidunt ipsa ipsum iusto maiores maxime nemo nihil nostrum nulla porro quam quas repellendus, reprehenderit sint sunt tempore temporibus veritatis! Consequatur, dolorum.</div>
                <div role="tabpanel" class="tab-pane" id="garantia">...</div>
                <div role="tabpanel" class="tab-pane" id="feedback">...</div>
            </div>
        </div>

<!--Slider here-->
    <div class="col-md-12">
        <span class="name_slider text-center "> Похожие товары </span>
    </div>

    <?php include("cart_item_prev.php"); ?>
    <?php include("cart_item_prev.php"); ?>
    <?php include("cart_item_prev.php"); ?>
    <?php include("cart_item_prev.php"); ?>
<!--    Slider2 here-->

    <div class="col-md-12">
        <span class="name_slider text-center "> Купите вместе с этим товаром</span>
    </div>

    <?php include("cart_item_prev.php"); ?>
    <?php include("cart_item_prev.php"); ?>
    <?php include("cart_item_prev.php"); ?>
    <?php include("cart_item_prev.php"); ?>

    <div class="col-md-12 hidden-xs hidden-sm">
        <div class="big_img "></div>
    </div>
    
    <div class="center-block row info_shop">
        <div class="col-sm-1">
            <img src="img/marker.png" alt="">
        </div>
        <div class="col-sm-11">
            Интернет-магазин МЮ мебель роботает для Вас наши дорогие поукпатели.
            Интернет-магазин МЮ мебель роботает для Вас наши дорогие поукпатели.
        </div>
    </div>




</div>





<?php include_once("footer.php"); ?>
    