<?php include_once("header.php") ?>

<div class="container">
    <div class="title text-center">
        <span>Оформнение заказа</span>
    </div>
    <div class="row ordering">
        <div class="col-md-6">
            <form action="">
                <p class="info_user">Полажуста, укажите Ваши контактные данниые:</p>

                <div class="col-sm-5 no_padding_left">
                    <label for="">ФИО*</label>
                    <input type="text" placeholder="Снигерев Вадим Петрович">
                    <input type="checkbox" class="subscribe_input" id="subscribe">
                    <label for="subscribe" class="subscribe_label">Подписаться на рассылку</label>
                </div>
                <div class="col-sm-4 no_padding_left">
                    <label for="">Email*</label>
                    <input type="email" placeholder="admin@mail.ru">
                </div>
                <div class="col-sm-3 no_padding_left">
                    <label for="">Телефон*</label>
                    <input type="text" placeholder="+ 3 999 999 9999">
                </div>
                <div class="clearfix"></div>
                <p class="info_user">Полажуста, укажите адрес доставки:</p>

                <div class="col-sm-5 no_padding_left">
                    <div class="form-group">
                        <label for="">Город</label>
                        <input type="text" placeholder="Москва">
                    </div>
                    <div class="form-group">
                        <label for="">Етаж</label>
                        <input type="text">
                    </div>
                </div>
                <div class="col-sm-4 no_padding_left">
                    <div class="form-group">
                        <label for="">Улица</label>
                        <input type="text" placeholder="10 я парковая">
                    </div>
                    <div class="form-group">
                        <label for="">Желательное время доставки</label>
                        <input type="text" placeholder="">
                    </div>
                </div>
                <div class="col-sm-3 no_padding">
                    <div class="form-group" style="display: inline-block">
                        <div class="col-xs-6 no_padding_left">
                            <label for="">Дом</label>
                            <input type="text" placeholder="21/1">
                        </div>
                        <div class="col-xs-6 no_padding_left">
                            <label for="">Кваритра</label>
                            <input type="text" placeholder="34">
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="checkbox" class="subscribe_input" id="elevator">
                        <label for="elevator" class="subscribe_label">В доме есть лифт</label>
                    </div>
                </div>
                <div class="col-xs-12 no_padding_left">
                    <label>Комментарий</label>
                    <input type="text" placeholder="Москва">
                </div>
                <div class="clearfix"></div>
                <div class="additional_services">
                    <p class="info_user">Дополнительные услуги:</p>

                    <div>
                        <input type="checkbox" class="subscribe_input" id="serv1">
                        <label for="serv1" class="subscribe_label">Подьем</label>
                    </div>
                    <div>
                        <input type="checkbox" class="subscribe_input" id="serv2">
                        <label for="serv2" class="subscribe_label">Сборка мебели: 1000 р</label>
                    </div>
                    <div>
                        <input type="checkbox" class="subscribe_input" id="serv3">
                        <label for="serv3" class="subscribe_label">Монтаж техники: стоимость данной услуги уточняйте
                            у менеджера</label>
                    </div>
                </div>
                <p class="info_user">Выберите способ оплаты:</p>

                <div class="payment">
                    <input type="radio" id="rad1" name="optradio"> <label for="rad1">Наличными при получении</label>
                    <input type="radio" id="rad2" name="optradio"> <label for="rad2">Банковской картой на сайте <br>
                        Visa/MasterCart
                        <img src="img/cart-visa.png" alt=""></label>
                </div>
                <div class="buttons_ordering">
                    <a class="back_to_cart" href="">&larr; Назад в корзину</a>
                    <button class="btn btn_by text-center" type="submit">оформить заказ</button>
                </div>
            </form>
        </div>
        
        <div class="col-md-offset-3 col-md-3 ">
            <div class="total_ticket row hidden-xs hidden-sm" style="margin-right: 0;">
                <img src="img/ordering.png" alt="">
                <span class="text-left title_ticket">Ваш заказ: </span>

                <div class="booking">
                        <div class="col-md-8 no_padding">
                            <ul class="list-unstyled">
                                <li>Кухня Мария Иоановна</li>
                                <li>Размер: 1600 x 1200 x 2100</li>
                                <li>Цвет: Дуб Венге/ Дуб Сонома/</li>
                                <li>Капучино</li>
                            </ul>
                        </div>
                        <div class="col-md-4 text-right no_padding_right">
                            <span class=" prise">6300 P</span>
                        </div>
                </div>

                <div class="booking">
                    <div class="col-md-8 no_padding">
                        <ul class="list-unstyled">
                            <li>Кухня Мария Иоановна</li>
                            <li>Размер: 1600 x 1200 x 2100</li>
                            <li>Цвет: Дуб Венге/ Дуб Сонома/</li>
                            <li>Капучино</li>
                        </ul>
                    </div>
                    <div class="col-md-4 text-right no_padding_right">
                        <span class=" prise">6300 P</span>
                    </div>
                </div>

                <div class="booking">
                    <div class="col-md-8 no_padding">
                        <ul class="list-unstyled">
                            <li>Кухня Мария Иоановна</li>
                            <li>Размер: 1600 x 1200 x 2100</li>
                            <li>Цвет: Дуб Венге/ Дуб Сонома/</li>
                            <li>Капучино</li>
                        </ul>
                    </div>
                    <div class="col-md-4 text-right no_padding_right">
                        <span class=" prise">6300 P</span>
                    </div>
                </div>


                <div class="total_prise_ticket">
                    <div class="col-md-8 text-left no_padding">
                        <span>Общая сума</span>
                        <br>
                        <span>Сумма скидки</span>
                    </div>
                    <div class="col-md-4 text-right no_padding_right ">
                        <span class=" prise">6300 P</span>
                        <span class=" prise">6300 P</span>
                    </div>
                    <div class="col-md-12">
                        <div class="great_total_prise">
                            Итого: <span> 32 500P</span>
                        </div>
                    </div>
                </div>

            </div>
        </div>




    </div>
    <div class="details text-left hidden-xs hidden-sm">
        <img src="img/details.png" alt="">
        Подробные условия:
    </div>

    <div class="specification row hidden-xs hidden-sm">
        <div class="col-md-3">
            <span>Доставка</span>
            <ul class="list-unstyled">
                <li><span class="bold_text">Доставка по Москве</span> - безплатно</li>
                <li><span class="bold_text">Доставка по МО за МКАД</span> - 100p/км</li>
                <li><span class="bold_text">Доставка по России</span> - до</li>
                <li>транспортной компании - безплатно</li>
            </ul>
        </div>
        <div class="col-md-3">
            <span>Ожидаемое время доставки</span>
            <ul class="list-unstyled">
                <li><span class="bold_text">Москва и область</span>- 1-2 дня</li>
                <li><span class="bold_text">По России</span>- 1-2 дней</li>
            </ul>
        </div>
        <div class="col-md-3">
            <span>Оплата </span>
            <ul class="list-unstyled">
                <li><span class="bold_text">Москва и МО</span> наличными при</li>
                <li>получении, безналичными</li>
                <li><span class="bold_text">Россия</span>- безналичный расчет</li>
            </ul>
        </div>
        <div class="col-md-3">
            <span>Гарантия 12 месяцев</span>
            <ul class="list-unstyled">
                <li><span class="bold_text">12 месяцев </span> официальной гарантии</li>
                <li>от производтеля</li>
                <li>Обмен/возврат товара в течении</li>
                <li>14 дней, при невскритой упаковке</li>
            </ul>
        </div>
    </div>
</div>
<?php include_once("footer.php") ?>

