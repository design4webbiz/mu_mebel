<?php include_once("header.php"); ?>

<div class="container korzina">
    <div class="bread_crums">
        <ol class="breadcrumb">
            <li><a href="#">Главная</a></li>

            <li class="active">Корзина</li>
        </ol>
    </div>

    <div class="title text-center">
        <span>Корзина</span>
    </div>

    <div class="title_decr">
        Доставляем мебель быстро,вся мебель на складе! Даем дополнительные гарантии на комплектность и качество товара.
    </div>

    <div class="product_title hidden-xs">

        <div class="col-md-7 text-left no_padding_left">
            <span>Описание товара</span>
        </div>
        <div class="col-md-1">
            <span>Цена</span>
        </div>
        <div class="col-md-2 text-center">
            <span>Количество</span>
        </div>
        <div class="col-md-2">
            <span class="sum">Общая сума</span>
        </div>

    </div>


    <div class="product_decr ">
        <div class="row">
            <div class="col-xs-12 col-md-2 col-sm-12 no_padding_left text-center">
                <img src="img/img_decr.png" alt="">
            </div>
            <div class="col-md-5 col-xs-12 col-sm-12">
                <ul class=" list-unstyled product_decr_list">
                    <li>Кухня Мария Иоановна</li>
                    <li>Размер:1600 x 1200 x 2100</li>
                    <li>Цвет: Дуб Венге / Дуб Сонома/</li>
                    <li>Капучино</li>
                </ul>
            </div>
            <div class="col-xs-4 col-xs-12 col-md-1 col-sm-2">

                <div class="prise_total hidden-xs hidden-sm">
                    11 300P<br>
                    <span>11 300P</span>
                </div>
            </div>
            <div class="col-xs-12 col-md-2 col-sm-12">
                <div class="total">

                    <button class="minus" style="line-height: 21px;
                            font-size: 80px;
                            padding-bottom: 18px;">
                        -
                    </button>

                    <input #id="count" type="text" value="1">

                    <button  class="plus">
                        +
                    </button>
                </div>

            </div>
            <div class="col-xs-12 col-md-1 ">

                <div class="prise_total ">
                    11 300P<br>
                    <span>11 300P</span>
                </div>

            </div>
            <div class="col-xs-12  col-md-1  text-center">
                <button id="delete">
                </button>
            </div>
        </div>
    </div>

    <div class="item_name text-left">
        Спальня Камчатские грезы
    </div>

    <div class="product_decr ">
        <div class="row">
            <div class="col-xs-12 col-md-2 col-sm-12 no_padding_left text-center">
                <img src="img/img_decr.png" alt="">
            </div>
            <div class="col-md-5 col-xs-12 col-sm-12">
                <ul class=" list-unstyled product_decr_list">
                    <li>Кухня Мария Иоановна</li>
                    <li>Размер:1600 x 1200 x 2100</li>
                    <li>Цвет: Дуб Венге / Дуб Сонома/</li>
                    <li>Капучино</li>
                </ul>
            </div>
            <div class="col-xs-12 col-md-1 col-sm-1">

                <div class="prise_total hidden-xs hidden-sm">
                    11 300P<br>
                    <span>11 300P</span>
                </div>
            </div>
            <div class="col-xs-12 col-md-2 ">
                <div class="total">

                    <button class="minus" style="line-height: 21px;
                            font-size: 80px;
                            padding-bottom: 18px;">
                        -
                    </button>

                    <input #id="count" type="text" value="1">

                    <button  class="plus">
                        +
                    </button>
                </div>

            </div>
            <div class="col-xs-12 col-md-1 ">

                <div class="prise_total ">
                    11 300P<br>
                    <span>11 300P</span>
                </div>

            </div>
            <div class="col-xs-12  col-md-1  text-center">
                <button id="delete">
                </button>
            </div>
        </div>
    </div>


    <div class="product_decr ">
        <div class="row">
            <div class="col-xs-12 col-md-2 col-sm-12 no_padding_left text-center">
                <img src="img/img_decr.png" alt="">
            </div>
            <div class="col-md-5 col-sm-12">
                <ul class=" list-unstyled product_decr_list">
                    <li>Кухня Мария Иоановна</li>
                    <li>Размер:1600 x 1200 x 2100</li>
                    <li>Цвет: Дуб Венге / Дуб Сонома/</li>
                    <li>Капучино</li>
                </ul>
            </div>
            <div class="col-xs-12 col-md-1 col-sm-1">

                <div class="prise_total hidden-xs hidden-sm">
                    11 300P<br>
                    <span>11 300P</span>
                </div>
            </div>
            <div class="col-xs-12 col-md-2 ">
                <div class="total">

                    <button class="minus" style="line-height: 21px;
                            font-size: 80px;
                            padding-bottom: 18px;">
                        -
                    </button>

                    <input #id="count" type="text" value="1">

                    <button  class="plus">
                        +
                    </button>
                </div>

            </div>
            <div class="col-xs-12 col-md-1 ">

                <div class="prise_total ">
                    11 300P<br>
                    <span>11 300P</span>
                </div>

            </div>
            <div class="col-xs-12  col-md-1  text-center">
                <button id="delete">
                </button>
            </div>
        </div>
    </div>

    <div class="product_decr ">
        <div class="row">
            <div class="col-xs-12 col-md-2 col-sm-12 no_padding_left text-center">
                <img src="img/img_decr.png" alt="">
            </div>
            <div class="col-md-5 col-xs-12 col-sm-12">
                <ul class=" list-unstyled product_decr_list">
                    <li>Кухня Мария Иоановна</li>
                    <li>Размер:1600 x 1200 x 2100</li>
                    <li>Цвет: Дуб Венге / Дуб Сонома/</li>
                    <li>Капучино</li>
                </ul>
            </div>
            <div class="col-md-1  col-xs-12">

                <div class="prise_total hidden-xs hidden-sm">
                    11 300P<br>
                    <span>11 300P</span>
                </div>
            </div>
            <div class="col-md-2 col-xs-12">
                <div class="total">

                    <button class="minus" style="line-height: 21px;
                            font-size: 80px;
                            padding-bottom: 18px;">
                        -
                    </button>

                    <input #id="count" type="text" value="1">

                    <button  class="plus">
                        +
                    </button>
                </div>

            </div>
            <div class="col-md-1 col-xs-12">

                <div class="prise_total">
                    11 300P<br>
                    <span>11 300P</span>
                </div>

            </div>
            <div class="col-xs-12  col-md-1  text-center">
                <button id="delete">
                </button>
            </div>
        </div>
    </div>


    <div class="product_decr ">
        <div class="row">
            <div class="col-xs-12 col-md-2 col-sm-12 no_padding_left text-center">
                <img src="img/img_decr.png" alt="">
            </div>
            <div class="col-md-5 col-xs-12 col-sm-12">
                <ul class=" list-unstyled product_decr_list">
                    <li>Кухня Мария Иоановна</li>
                    <li>Размер:1600 x 1200 x 2100</li>
                    <li>Цвет: Дуб Венге / Дуб Сонома/</li>
                    <li>Капучино</li>
                </ul>
            </div>
            <div class="col-xs-12 col-md-1 col-sm-1">

                <div class="prise_total hidden-xs hidden-sm">
                    11 300P<br>
                    <span>11 300P</span>
                </div>
            </div>
            <div class="col-xs-12 col-md-2 ">
                <div class="total">

                    <button class="minus" style="line-height: 21px;
                            font-size: 80px;
                            padding-bottom: 18px;">
                        -
                    </button>

                    <input #id="count" type="text" value="1">

                    <button  class="plus">
                        +
                    </button>
                </div>

            </div>
            <div class="col-xs-12 col-md-1 ">

                <div class="prise_total ">
                    11 300P<br>
                    <span>11 300P</span>
                </div>

            </div>
            <div class="col-xs-12  col-md-1  text-center">
                <button id="delete">
                </button>
            </div>
        </div>
    </div>

    <div class="total_prise text-right ">
        <ul class="list-unstyled">
            <li>Итого: <span>32 500P</span></li>
            <li>Предварительная дата доставки 20 сентября 2016р</li>
            <li>Общая сумма: 32 500Р</li>
            <li>Сумма скидки: 6 000P</li>
        </ul>
        <button>
            Оформить заказ
        </button>
    </div>

    <div class="details text-left hidden-sm hidden-xs">
        <img src="img/details.png" alt="">
        Подробные условия:
    </div>



    <div class="specification row hidden-sm hidden-xs">
        <div class="col-md-3">
            <span>Доставка</span>
            <ul class="list-unstyled">
                <li><span class="bold_text">Доставка по Москве</span> - безплатно</li>
                <li><span class="bold_text">Доставка по МО за МКАД</span> - 100p/км</li>
                <li><span class="bold_text">Доставка по России</span> - до</li>
                <li>транспортной компании - безплатно</li>
            </ul>
        </div>
        <div class="col-md-3">
            <span>Ожидаемое время доставки</span>
            <ul class="list-unstyled">
                <li><span class="bold_text">Москва и область</span>- 1-2 дня</li>
                <li><span class="bold_text">По России</span>- 1-2 дней</li>
            </ul>
        </div>
        <div class="col-md-3">
            <span>Оплата </span>
            <ul class="list-unstyled">
                <li><span class="bold_text">Москва и МО</span> наличными при</li>
                <li>получении, безналичными</li>
                <li><span class="bold_text">Россия</span>- безналичный расчет</li>
            </ul>
        </div>
        <div class="col-md-3">
            <span>Гарантия 12 месяцев</span>
            <ul class="list-unstyled">
                <li><span class="bold_text">12 месяцев </span> официальной гарантии</li>
                <li>от производтеля</li>
                <li>Обмен/возврат товара в течении</li>
                <li>14 дней, при невскритой упаковке</li>
            </ul>
        </div>
    </div>

</div>

<?php include_once("footer.php"); ?>