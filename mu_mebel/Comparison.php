<?php include_once("header.php"); ?>



<div class="container">
    <div class="bread_crums">
        <ol class="breadcrumb">
            <li><a href="#">Главная</a></li>

            <li class="active">Сравнение</li>
        </ol>
    </div>

    <div class="title text-center">
        <span>Сравнение</span>
    </div>
    <div class="col-md-3">
        <div class="compare_list">
            <ul class="list-unstyled">
                <li>В списке 8 кухонь</li>
                <li></li>
                <li></li>
                <li></li>
            </ul>

            <a href="#">Очистить список</a>
        </div>

        <div class="ul list-unstyled title_compare_item ">
            <li>Цвет исполнения</li>
            <li>Метериалы</li>
            <li>Состав комплекта</li>
        </div>

    </div>
    <div class="col-md-9">
        <div class="compare_item_list">
            <ul class="list-unstyled list-inline">
                <li class="active">Кухни <span>8</span></li>
                <li>Спальни <span>8</span></li>
                <li>Кровати <span>12</span></li>
                <li>Тумбы <span>4</span></li>
            </ul>
        </div>
<!--slider here-->
        <div class="col-md-4">
           <div class="compare_item">
               <img src="img/compare_img.png" alt="">
               <div class="top_section">
                   <span class="compare_item_name">Кухня Мария Иоановна</span>
                   <br>
                   <span class="compare_item_price">125 000Р</span>
                   <button class="compare_item_price_button">Купить</button>
               </div>
               <span class="compare_data">Дуб Венге / Дуб Сонома</span>
               <ul class="list-unstyled compare_data">
                   <li>ЛДСП 22 мм и 16 мм</li>
                   <li>МДФ 16 мм</li>
                   <li>Кант ПВХ 2 мм и 0,4 мм</li>
                   <li>Постформинг 38 мм</li>
               </ul>

               <ul class="compare_consist list-unstyled">
                   <li>Комод Витязь
                       <br>1000 х 1000 х 400 </li>
                   <li>Шкаф-пенал Матрешка
                       <br>1000 х 1000 х 400 </li>
               </ul>
                    <button class="compare_item_price_button_2 text-center">Купить</button>
           </div>
        </div>


        <div class="col-md-4">
            <div class="compare_item">
                <img src="img/compare_img.png" alt="">
                <div class="top_section">
                    <span class="compare_item_name">Кухня Мария Иоановна</span>
                    <br>
                    <span class="compare_item_price">125 000Р</span>
                    <button class="compare_item_price_button">Купить</button>
                </div>
                <span class="compare_data">Дуб Венге / Дуб Сонома</span>
                <ul class="list-unstyled compare_data">
                    <li>ЛДСП 22 мм и 16 мм</li>
                    <li>МДФ 16 мм</li>
                    <li>Кант ПВХ 2 мм и 0,4 мм</li>
                    <li>Постформинг 38 мм</li>
                </ul>

                <ul class="compare_consist list-unstyled">
                    <li>Комод Витязь
                        <br>1000 х 1000 х 400 </li>
                    <li>Шкаф-пенал Матрешка
                        <br>1000 х 1000 х 400 </li>
                </ul>
                <button class="compare_item_price_button_2 text-center">Купить</button>
            </div>
        </div>


        <div class="col-md-4">
            <div class="compare_item">
                <img src="img/compare_img.png" alt="">
                <div class="top_section">
                    <span class="compare_item_name">Кухня Мария Иоановна</span>
                    <br>
                    <span class="compare_item_price">125 000Р</span>
                    <button class="compare_item_price_button">Купить</button>
                </div>
                <span class="compare_data">Дуб Венге / Дуб Сонома</span>
                <ul class="list-unstyled compare_data">
                    <li>ЛДСП 22 мм и 16 мм</li>
                    <li>МДФ 16 мм</li>
                    <li>Кант ПВХ 2 мм и 0,4 мм</li>
                    <li>Постформинг 38 мм</li>
                </ul>

                <ul class="compare_consist list-unstyled">
                    <li>Комод Витязь
                        <br>1000 х 1000 х 400 </li>
                    <li>Шкаф-пенал Матрешка
                        <br>1000 х 1000 х 400 </li>
                </ul>
                <button class="compare_item_price_button_2 text-center">Купить</button>
            </div>
        </div>

       </div>

    <div class="center-block row info_shop">
        <div class="col-sm-1">
            <img src="img/marker.png" alt="">
        </div>
        <div class="col-sm-11">
            Интернет-магазин МЮ мебель роботает для Вас наши дорогие поукпатели.
            Интернет-магазин МЮ мебель роботает для Вас наши дорогие поукпатели.
        </div>
    </div>

</div>


<?php include_once("footer.php"); ?>
