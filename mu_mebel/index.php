<?php include_once("header.php"); ?>

<div class="content">
    <div class="container">
        <div class="category height_block_1 hidden-sm hidden-xs">
            <ul class="list-unstyled">
                <li><a href=""><img src="img/01.png" alt=""> Столы до 2000</a></li>
                <li><a href=""><img src="img/02.png" alt=""> Диваны до 8000</a></li>
                <li><a href=""><img src="img/02.png" alt=""> Диваны до 30000</a></li>
                <li><a href=""><img src="img/03.png" alt=""> Кровати до 50000</a></li>
                <li><a href=""><img src="img/04.png" alt=""> Кровати до 10000</a></li>
                <li><a href=""><img src="img/05.png" alt=""> Шкафы до 30000</a></li>
                <li><a href=""><img src="img/06.png" alt=""> Шкафы-купе до 30000</a></li>
            </ul>
        </div>

        <div class="head_banner height_block_2">
            <img src="img/banner.png" alt="">
        </div>
        <div class="head_block_delivery height_block_3 hidden-sm hidden-xs">
            <p class="title">везем заказы быстро:</p>

            <div class="img_block">
                <img class="img-responsive center-block" src="img/car.png" alt="">
            </div>
            <div class="text_block">
                По Москве 1 день <br>
                По России 5 дней
            </div>
            <p class="title">оплату берем по факту:</p>

            <div class="img_block">
                <img class="img-responsive center-block" src="img/money.png" alt="">
            </div>
            <div class="text_block">
                Москва по факту доставки <br>
                Россия по факту отгрузки
            </div>
            <p class="title">сохраняєм гарантию:</p>

            <div class="img_block">
                <img class="img-responsive center-block" src="img/img-icon.png" alt="">
            </div>
            <div class="text_block">
                Гарантия от 1 года <br>
                Гарантия после сборки
            </div>
        </div>
        <div class="center-block discount">
            <form class="text-center" action="">
                <label for="">Хотите получить товары со скидкой?</label>
                <input type="email" placeholder="Введите Ваш e-mail">
                <input type="submit" value="Подписаться">
            </form>
        </div>
        <div class="actions">
            <h3>акции:</h3>
            <hr class="border">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="one_product">
                        <img src="img/action.png" alt="">

                        <span class="title">Кухня Мария Ивановна</span> <br>

                        <span class="price">125 000 P</span>
                        <button class="btn btn_by">купить</button>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="one_product">
                        <img src="img/action.png" alt="">

                        <span class="title">Кухня Мария Ивановна</span> <br>

                        <span class="price">125 000 P</span>
                        <button class="btn btn_by">купить</button>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="one_product">
                        <img src="img/action.png" alt="">

                        <span class="title">Кухня Мария Ивановна</span> <br>

                        <span class="price">125 000 P</span>
                        <button class="btn btn_by">купить</button>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="one_product">
                        <img src="img/action.png" alt="">

                        <span class="title">Кухня Мария Ивановна</span> <br>

                        <span class="price">125 000 P</span>
                        <button class="btn btn_by ">купить</button>
                    </div>
                </div>
            </div>
            <hr class="border">
        </div>
        <div class="willing_offer">
            <h3>готовые предложения:</h3>

            <div class="row">
                <div class="col-md-4 col-sm-4">
                    <div class="one_product">
                        <div style="border: 1px solid #ccc;">
                            <img src="img/img1.png" alt="">

                            <p class="title">Кухни от 120 тысяч рублей</p>
                            <button class="btn view center-block">Смотреть</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="one_product">
                        <div style="border: 1px solid #ccc;">
                            <img src="img/img1.png" alt="">

                            <p class="title">Кухни от 120 тысяч рублей</p>
                            <button class="btn view center-block">Смотреть</button>
                        </div>
                    </div>
                </div>
                 <div class="col-md-4 col-sm-4">
                    <div class="one_product">
                        <div style="border: 1px solid #ccc;">
                            <img src="img/img1.png" alt="">

                            <p class="title">Кухни от 120 тысяч рублей</p>
                            <button class="btn view center-block">Смотреть</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="center-block  info_shop row ">
            <div class="col-sm-1">
                <img src="img/marker.png" alt="">
            </div>
            <div class="col-sm-11">
                Интернет-магазин МЮ мебель роботает для Вас наши дорогие поукпатели.
                Интернет-магазин МЮ мебель роботает для Вас наши дорогие поукпатели.
            </div>
        </div>
    </div>
</div>

<?php include_once("footer.php"); ?>


</body>
</html>