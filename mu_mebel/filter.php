<?php include_once("header.php"); ?>

<script>
    $( function() {
        $( "#slider-range" ).slider({
            range: true,
            min: 0,
            max: 500,
            values: [ 75, 300 ],
            slide: function( event, ui ) {
                $( "#amount" ).val( "$" + ui.values[ 0 ]  );
                $( "#amount_2" ).val( "$" + ui.values[ 1 ]  );
            }
        });
        $( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 )  );
        $( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 1 )  );
    } );
</script>



    <div class="container">
        <div class="bread_crums">
            <ol class="breadcrumb">
                <li><a href="#">Главная</a></li>

                <li class="active">Кухни</li>
            </ol>
        </div>
        <div class="title text-center">
            <span>Кухня</span>
        </div>

        <div class="descr_page">
            Мы рады предложить вам красивие и качествынние кухни для Вашего любимого дома. Обратите внимание на наш широкий асортимент,
             отличное качество мебели и огромное желание решить ваши проблемы.
        </div>

        <div class="col-md-3 ">

            <div class="filter_left_sidebar">

                <span class="filter_name">Геметрия</span>
                <button type="button" class="btn btn-default" data-toggle="tooltip" data-placement="left" title="Подсказка"></button>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" value="">
                        Прямая
                    </label>

                </div>

                <div class="checkbox">
                    <label>
                        <input type="checkbox" value="">
                      Угловая
                    </label>

                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" value="">
                        П-образная
                    </label>

                </div>

               <div class="line "></div>

                <span class="filter_name">Стиль</span>
                <button type="button" class="btn btn-default"
                        data-toggle="tooltip" data-placement="left" title="Подсказка">  </button>

                <select class="form-control">
                    <option>Первый </option>
                    <option>Второй</option>
                    <option>Третий</option>
                </select>

                <div class="line"></div>


                <div class="scroll_slider">
                <span class="filter_name">Цена</span>

                <form class="form-inline" role="form">
                    <div class="form-group">
                        <input type="text" id="amount" class="form-control" value="от" id="amount" style="margin-right: 10px;" >
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" value="до" id="amount_2">
                    </div>
                </form>

                <div id="slider-range"></div>
                    </div>


                <div class="line"></div>

                <span class="filter_name">Габариты</span>
                <button type="button" class="btn btn-default"
                        data-toggle="tooltip" data-placement="left" title="Подсказка">  </button>
                <input type="text" >

                <div class="line"></div>

                <span class="filter_name">Цвет</span>
                <select class="form-control">
                    <option></option>
                    <option>Первый </option>
                    <option>Второй</option>
                    <option>Третий</option>
                </select>

                <div class="line"></div>

                <span class="filter_name">Площадь кухни,кв.м.</span>
                <select class="form-control">
                    <option></option>
                    <option>Первый </option>
                    <option>Второй</option>
                    <option>Третий</option>
                </select>

                <div class="line"></div>

                <span class="filter_name">Декор</span>
                <button type="button" class="btn btn-default"
                        data-toggle="tooltip" data-placement="left" title="Подсказка">  </button>

                <select class="form-control">
                    <option></option>
                    <option>Первый </option>
                    <option>Второй</option>
                    <option>Третий</option>
                </select>
                <div class="line"></div>


                <span class="filter_name">Производитель</span>
                <button type="button" class="btn btn-default"
                        data-toggle="tooltip" data-placement="left" title="Предприятие изготовитель даной линейки мебели"> </button>

                <select class="form-control">
                    <option></option>
                    <option>Первый </option>
                    <option>Второй</option>
                    <option>Третий</option>
                </select>

                <div class="line"></div>

                <a href="#">Сбросить параметры</a>



            </div>


        </div>

        <div class="col-md-9 ">
            <div class="tag display_none hidden-sm hidden-xs">
                <ul class="list-unstyled list-inline">
                    <li><a href="#">Купить кухню от производителя </a></li>
                    <li><a href="#">Эконом кухни</a></li>
                    <li><a href="#">Стол обеденний для кухни</a></li>
                    <li><a href="#">Стол обеденний для кухни</a></li>
                    <li><a href="#">Кухонный уголок для кухни</a></li>

                    <li><a href="#">Купить кухню от производителя </a></li>
                    <li><a href="#">Эконом кухни</a></li>
                    <li><a href="#">Стол обеденний для кухни</a></li>
                    <li><a href="#">Стол обеденний для кухни</a></li>
                    <li><a href="#">Кухонный уголок для кухни</a></li>
                </ul>
            </div>
            <div class="search_panel ">
                <div class="col-md-4 no_padding_left ">
                    <span >Найдено <span>1127</span> товаров</span>
                </div>
                <div class="col-md-4  col-md-push-1 no_padding_left">
                    <div class="chexbox_block text-right">
                        <label class="checkbox-inline"><input type="checkbox" value="">Акции</label>
                        <label class="checkbox-inline"><input type="checkbox" value="">Хиты</label>
                        <label class="checkbox-inline"><input type="checkbox" value="">Новинки</label>
                    </div>
                </div>
                <div class="col-md-3  col-md-push-1  no_padding">
                    <span class="sort">Сортировать: <span>Цене &uarr;</span></span>
                </div>
            </div>

            <div class="massive_items_results ">
                <div class="col-md-4  col-sm-6">
                    <div class="ware_cards">
                        <img src="img/compare_img.png" alt="">
                        <span class="ware_cards_item_name">Кухня Мария Иоановна</span>
                        <br>
                        <span class="ware_cards_item_price">125 000Р</span>
                        <button class="ware_cards_item_price_button">Купить</button>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6">
                    <div class="ware_cards">
                        <img src="img/compare_img.png" alt="">
                        <span class="ware_cards_item_name">Кухня Мария Иоановна</span>
                        <br>
                        <span class="ware_cards_item_price">125 000Р</span>
                        <button class="ware_cards_item_price_button">Купить</button>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6">
                    <div class="ware_cards">
                        <img src="img/compare_img.png" alt="">
                        <span class="ware_cards_item_name">Кухня Мария Иоановна</span>
                        <br>
                        <span class="ware_cards_item_price">125 000Р</span>
                        <button class="ware_cards_item_price_button">Купить</button>
                    </div>
                </div>


                <div class="col-md-4 col-sm-6">
                    <div class="ware_cards">
                        <img src="img/compare_img.png" alt="">
                        <span class="ware_cards_item_name">Кухня Мария Иоановна</span>
                        <br>
                        <span class="ware_cards_item_price">125 000Р</span>
                        <button class="ware_cards_item_price_button">Купить</button>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6">
                    <div class="ware_cards">
                        <img src="img/compare_img.png" alt="">
                        <span class="ware_cards_item_name">Кухня Мария Иоановна</span>
                        <br>
                        <span class="ware_cards_item_price">125 000Р</span>
                        <button class="ware_cards_item_price_button">Купить</button>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6">
                    <div class="ware_cards">
                        <img src="img/compare_img.png" alt="">
                        <span class="ware_cards_item_name">Кухня Мария Иоановна</span>
                        <br>
                        <span class="ware_cards_item_price">125 000Р</span>
                        <button class="ware_cards_item_price_button">Купить</button>
                    </div>
                </div>


                <div class="col-md-4 col-sm-6">
                    <div class="ware_cards">
                        <img src="img/compare_img.png" alt="">
                        <span class="ware_cards_item_name">Кухня Мария Иоановна</span>
                        <br>
                        <span class="ware_cards_item_price">125 000Р</span>
                        <button class="ware_cards_item_price_button">Купить</button>
                    </div>
                </div>


                <div class="col-md-4 col-sm-6">
                    <div class="ware_cards">
                        <img src="img/compare_img.png" alt="">
                        <span class="ware_cards_item_name">Кухня Мария Иоановна</span>
                        <br>
                        <span class="ware_cards_item_price">125 000Р</span>
                        <button class="ware_cards_item_price_button">Купить</button>
                    </div>
                </div>


                <div class="col-md-4 col-sm-6">
                    <div class="ware_cards">
                        <img src="img/compare_img.png" alt="">
                        <span class="ware_cards_item_name">Кухня Мария Иоановна</span>
                        <br>
                        <span class="ware_cards_item_price">125 000Р</span>
                        <button class="ware_cards_item_price_button">Купить</button>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6">
                    <div class="ware_cards">
                        <img src="img/compare_img.png" alt="">
                        <span class="ware_cards_item_name">Кухня Мария Иоановна</span>
                        <br>
                        <span class="ware_cards_item_price">125 000Р</span>
                        <button class="ware_cards_item_price_button">Купить</button>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6">
                    <div class="ware_cards">
                        <img src="img/compare_img.png" alt="">
                        <span class="ware_cards_item_name">Кухня Мария Иоановна</span>
                        <br>
                        <span class="ware_cards_item_price">125 000Р</span>
                        <button class="ware_cards_item_price_button">Купить</button>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6">
                    <div class="ware_cards">
                        <img src="img/compare_img.png" alt="">
                        <span class="ware_cards_item_name">Кухня Мария Иоановна</span>
                        <br>
                        <span class="ware_cards_item_price">125 000Р</span>
                        <button class="ware_cards_item_price_button">Купить</button>
                    </div>
                </div>


            </div>



        </div>

        <div class="descr_page col-sm-12 ">
            Мы рады предложить вам красивие и качествынние кухни для Вашего любимого дома. Обратите внимание на наш широкий асортимент,
            отличное качество мебели и огромное желание решить ваши проблемы.
        </div>

        <div class="tag col-sm-12 ">
            <ul class="list-unstyled list-inline">
                <li><a href="#">Купить кухню от производителя </a></li>
                <li><a href="#">Эконом кухни</a></li>
                <li><a href="#">Стол обеденний для кухни</a></li>
                <li><a href="#">Стол обеденний для кухни</a></li>
                <li><a href="#">Кухонный уголок для кухни</a></li>
                <li><a href="#">Стол обеденний для кухни</a></li>

            </ul>




    </div>
        </div>



<?php include_once("footer.php"); ?>
