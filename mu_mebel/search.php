<?php include_once("header.php"); ?>

<div class="container">
    <div class="bread_crums">
        <ol class="breadcrumb">
            <li><a href="#">Главная</a></li>

            <li class="active">Кухни</li>
        </ol>
    </div>
    <div class="title text-center">
        <span>Результаты поиска по запросу</span>
    </div>

    <div class="input_search text-center">

        <input type="text" >
        <img src="img/search_icon.png" alt="">
    </div>

    <div class="search_panel ">
        <div class="col-md-4 col-sm-12 ">
            <span >Найдено <span>1127</span> товаров</span>
        </div>
        <div class="col-md-4 col-md-push-2 col-sm-12">
            <div class="chexbox_block text-right">
                <label class="checkbox-inline"><input type="checkbox" value="">Акции</label>
                <label class="checkbox-inline"><input type="checkbox" value="">Хиты</label>
                <label class="checkbox-inline"><input type="checkbox" value="">Новинки</label>
            </div>
        </div>
<div class="col-md-2 col-md-push-2 col-sm-12     ">
        <span class="sort">Сортировать: <span>Цене &uarr;</span></span>
</div>
    </div>
    
    <div class="massive_items_results">
        <div class="col-md-3 col-sm-6">
            <div class="ware_cards">
                <img src="img/compare_img.png" alt="">
                    <span class="ware_cards_item_name">Кухня Мария Иоановна</span>
                    <br>
                    <span class="ware_cards_item_price">125 000Р</span>
                    <button class="ware_cards_item_price_button">Купить</button>
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="ware_cards">
                <img src="img/compare_img.png" alt="">
                <span class="ware_cards_item_name">Кухня Мария Иоановна</span>
                <br>
                <span class="ware_cards_item_price">125 000Р</span>
                <button class="ware_cards_item_price_button">Купить</button>
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="ware_cards">
                <img src="img/compare_img.png" alt="">
                <span class="ware_cards_item_name">Кухня Мария Иоановна</span>
                <br>
                <span class="ware_cards_item_price">125 000Р</span>
                <button class="ware_cards_item_price_button">Купить</button>
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="ware_cards">
                <img src="img/compare_img.png" alt="">
                <span class="ware_cards_item_name">Кухня Мария Иоановна</span>
                <br>
                <span class="ware_cards_item_price">125 000Р</span>
                <button class="ware_cards_item_price_button">Купить</button>
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="ware_cards">
                <img src="img/compare_img.png" alt="">
                <span class="ware_cards_item_name">Кухня Мария Иоановна</span>
                <br>
                <span class="ware_cards_item_price">125 000Р</span>
                <button class="ware_cards_item_price_button">Купить</button>
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="ware_cards">
                <img src="img/compare_img.png" alt="">
                <span class="ware_cards_item_name">Кухня Мария Иоановна</span>
                <br>
                <span class="ware_cards_item_price">125 000Р</span>
                <button class="ware_cards_item_price_button">Купить</button>
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="ware_cards">
                <img src="img/compare_img.png" alt="">
                <span class="ware_cards_item_name">Кухня Мария Иоановна</span>
                <br>
                <span class="ware_cards_item_price">125 000Р</span>
                <button class="ware_cards_item_price_button">Купить</button>
            </div>
        </div>
        <div class="col-md-3 col-sm-6">
            <div class="ware_cards">
                <img src="img/compare_img.png" alt="">
                <span class="ware_cards_item_name">Кухня Мария Иоановна</span>
                <br>
                <span class="ware_cards_item_price">125 000Р</span>
                <button class="ware_cards_item_price_button">Купить</button>
            </div>
        </div>


    </div>
<div class="pagination_block text-center">
    <ul class="pagination ">
        <li class="disabled"><a href="#"> < </a></li>
        <li><a href="#">1</a></li>
        <li><a href="#">2</a></li>
        <li><a href="#">3</a></li>
        <li><a href="#">4</a></li>
        <li><a href="#">5</a></li>
        <li class="disabled"><a href="#"> > </a></li>

    </ul>
</div>

    <div class="center-block row info_shop">
        <div class="col-sm-1">
            <img src="img/marker.png" alt="">
        </div>
        <div class="col-sm-11">
            Интернет-магазин МЮ мебель роботает для Вас наши дорогие покупатели.
            Интернет-магазин МЮ мебель роботает для Вас наши дорогие покупатели.
        </div>
    </div>

</div>

<?php include_once("footer.php"); ?>
