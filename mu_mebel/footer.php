
<footer>
    <div class="container">
        <ul class="list-inline media_support_class ">
            <li>Кухни
                <ul class="list-unstyled f_title">
                    <li><a href="#">Кухни помодульно</a></li>
                    <li><a href="#">Готовые кухни</a></li>
                    <li><a href="#">Кухонные решения</a></li>
                    <li><a href="#">Кухонные уголки</a></li>
                    <li><a href="#">Дорогие кухни</a></li>
                </ul>
            </li>
            <li>Спальни
                <ul class="list-unstyled f_title">
                    <li><a href="#">Готовые спальни</a></li>
                    <li><a href="#">Спальни помодульно</a></li>
                    <li><a href="#">Спальные решения</a></li>
                    <li><a href="#">Кровати</a></li>
                    <li><a href="#">Шкафы</a></li>
                    <li><a href="#">Тумбы</a></li>
                    <li><a href="#">Туалетные столики</a></li>
                    <li><a href="#">Диваны</a></li>
                    <li><a href="#">Полки</a></li>
                    <li><a href="#">Тв тумбы</a></li>
                    <li><a href="#">Столы</a></li>
                    <li><a href="#">Зеркала</a></li>
                    <li><a href="#">Пуфы</a></li>
                    <li><a href="#">Матрасы</a></li>
                </ul>
            </li>
            <li>Гостинная
                <ul class="list-unstyled f_title">
                    <li><a href="#">Готовые гостиные</a></li>
                    <li><a href="#">Модульные гостиные</a></li>
                    <li><a href="#">Гостиные решения</a></li>
                    <li><a href="#">Шкафы</a></li>
                    <li><a href="#">Тумбы ТВ</a></li>
                    <li><a href="#">Диваны</a></li>
                    <li><a href="#">Полки</a></li>
                    <li><a href="#">Столы</a></li>
                    <li><a href="#">Туалетные столики</a></li>
                    <li><a href="#">Комоды</a></li>
                </ul>
            </li>
            <li>Прихожая
                <ul class="list-unstyled f_title">
                    <li><a href="#">Модульные прихожие</a></li>
                    <li><a href="#">Готовые прихожие</a></li>
                    <li><a href="#">Прихожие решения</a></li>
                    <li><a href="#">Тумбы для обуви</a></li>
                    <li><a href="#">Комоды</a></li>
                    <li><a href="#">Пуфы</a></li>
                    <li><a href="#">Полки</a></li>
                    <li><a href="#">Стележы</a></li>
                    <li><a href="#">Зеркала</a></li>
                    <li><a href="#">Туалетные столики</a></li>
                </ul>
            </li>
            <li>Детская
                <ul class="list-unstyled f_title">
                    <li><a href="#">Модульные детские</a></li>
                    <li><a href="#">Готовые детские</a></li>
                    <li><a href="#">Детские решения</a></li>
                    <li><a href="#">Шкафы</a></li>
                    <li><a href="#">Стенки</a></li>
                    <li><a href="#">Тумбы ТВ</a></li>
                    <li><a href="#">Диваны</a></li>
                    <li><a href="#">Полки</a></li>
                    <li><a href="#">Столы</a></li>
                    <li><a href="#">Туалетные столики</a></li>
                    <li><a href="#">Кровати</a></li>
                    <li><a href="#">Зеркала</a></li>
                    <li><a href="#">Пуфы</a></li>
                    <li><a href="#">Матрасы</a></li>
                </ul>
            </li>
            <li>Подростковая
                <ul class="list-unstyled f_title">
                    <li><a href="#">Модульные подростковые</a></li>
                    <li><a href="#">Готовые подростковые</a></li>
                    <li><a href="#">Подростковые решения</a></li>
                    <li><a href="#">Шкафы</a></li>
                    <li><a href="#">Стенки</a></li>
                    <li><a href="#">Тумбы ТВ</a></li>
                    <li><a href="#">Диваны</a></li>
                    <li><a href="#">Полки</a></li>
                    <li><a href="#">Столы</a></li>
                    <li><a href="#">Туалетные столики</a></li>
                    <li><a href="#">Кровати</a></li>
                    <li><a href="#">Зеркала</a></li>
                    <li><a href="#">Пуфы</a></li>
                    <li><a href="#">Матрасы</a></li>
                </ul>
            </li>
            <li>Кабинет
                <ul class="list-unstyled f_title">
                    <li><a href="#">Модульные кабинеты</a></li>
                    <li><a href="#">Готовые кабинеты</a></li>
                    <li><a href="#">Кабинетные решения</a></li>
                    <li><a href="#">Шкафы</a></li>
                    <li><a href="#">Стенки</a></li>
                    <li><a href="#">Тумбы ТВ</a></li>
                    <li><a href="#">Диваны</a></li>
                    <li><a href="#">Полки</a></li>
                    <li><a href="#">Столы</a></li>
                    <li><a href="#">Туалетные столики</a></li>
                    <li><a href="#">Кровати</a></li>
                    <li><a href="#">Зеркала</a></li>
                    <li><a href="#">Пуфы</a></li>
                    <li><a href="#">Матрасы</a></li>
                </ul>
            </li>
            <li>Пердметы
                <ul class="list-unstyled f_title">
                    <li><a href="#">Полутороспальные кровати </a></li>
                    <li><a href="#">Односпальные кровати</a></li>
                    <li><a href="#">Двуспальные кровати</a></li>
                    <li><a href="#">Шкафы</a></li>
                    <li><a href="#">Стенки</a></li>
                    <li><a href="#">Тумбы ТВ</a></li>
                    <li><a href="#">Диваны</a></li>
                    <li><a href="#">Полки</a></li>
                    <li><a href="#">Столы</a></li>
                    <li><a href="#">Туалетные столики</a></li>
                    <li><a href="#">Кровати</a></li>
                    <li><a href="#">Зеркала</a></li>
                    <li><a href="#">Пуфы</a></li>
                    <li><a href="#">Матрасы</a></li>
                </ul>
            </li>
        </ul>




    </div>
    <div class="container f_container">

        <div class="both_footer row">
            <div class="col-md-2 logo"><img src="img/logo.png" alt="logo"></div>
            <div class="col-md-3  slogan">
                доставка мебели<br> по москве и <br>России
            </div>
            <div class="col-md-7 col-sm-12 list">
                <ul class="list-inline f_list">
                    <li>О Компании
                        <ul class="list-unstyled f_list_in">
                            <li><a href="#">Услуги</a></li>
                            <li><a href="#">Доставка и оплата</a></li>
                            <li><a href="#">Статьи</a></li>
                            <li><a href="#">Новости</a></li>
                        </ul>
                    </li>

                    <li>Покупателям
                        <ul class="list-unstyled f_list_in ">
                            <li><a href="#">Вопросы и ответы</a></li>
                            <li><a href="#">ЗD программа</a></li>

                        </ul>
                    </li>

                    <li>Партнерам
                        <ul class="list-unstyled f_list_in ">
                            <li><a href="#">Стать партнером</a></li>
                        </ul>
                    </li>
                    <li class="text-left">Контакты
                        <ul class="list-unstyled f_list_in ">
                            <li>г.Москва, Красносельcкая 12</li>
                            <li>тел.: 8-916-564-53-56</li>
                            <li>Время работы: с 11 до 22</li>
                        </ul>
                    </li>
                </ul>

            </div>



            <div class=" soc_list col-sm-12 col-md-3">
                <p>Присоидиняйтесь к нам:</p>
                <ul class="list-inline list-unstyled">
                    <li><a href="href=#"><img src="img/f_vk.png" alt="vk"></a></li>
                    <li><a href="href=#"><img src="img/f_ok.png" alt="ok"></a></li>
                    <li><a href="href=#"><img src="img/f_fb.png" alt="fb"></a></li>
                    <li><a href="href=#"><img src="img/f_tw.png" alt="fb"></a></li>
                    <li><a href="href=#"><img src="img/f_gl.png" alt="google"></a></li>
                    <li><a href="href=#"><img src="img/f_sm.png" alt="sm"></a></li>
                </ul>
            </div>




        </div>
        <div class="text-center copy">
            <span >2015-2016 интернет магазин МЮ мебель</span>
        </div>
    </div>

</footer>