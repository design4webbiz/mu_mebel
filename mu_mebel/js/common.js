$(document).ready(function() {

   if ($(window).width() <=1500 && $(window).width() >= 992){
        var height1 = $(".height_block_1").height();
        $(".height_block_2, .height_block_3").height(height1 - 2);
        $(".height_block_3").css("padding-top", height1 / 20);
    }







    $('.total .plus').on('click', function () {
        var valInput = parseInt($(this).prev('input').val());

        if(valInput >= 99){
            var valInputNew  =  valInput;
            $(this).prev('input').val(valInputNew);
        }else {
            var valInputNew  =  valInput + 1;
            $(this).prev('input').val(valInputNew);
        }
       
    });

    $('.total .minus').on('click', function () {
        var valInput = parseInt($(this).next('input').val());

        if(valInput <= 1){
            var valInputNew  =  valInput;
            $(this).next('input').val(valInputNew);
         }
        else {
            var valInputNew  =  valInput - 1;
            $(this).next('input').val(valInputNew);
        }


    });


});
