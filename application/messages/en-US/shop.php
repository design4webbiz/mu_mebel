<?php
/**
 * @package    oakcms
 * @author     Hryvinskyi Volodymyr <script@email.ua>
 * @copyright  Copyright (c) 2015 - 2017. Hryvinskyi Volodymyr
 * @version    0.0.1-beta.0.1
 */

return [
    'Amount' => '',
    'Available' => '',
    'Category' => '',
    'Category ID' => '',
    'Close' => '',
    'Filter' => '',
    'ID' => '',
    'Image' => '',
    'Modifications soted' => '',
    'Name' => '',
    'Price' => '',
    'Product ID' => '',
    'Product Modifications' => '',
    'Select Category' => '',
    'Select category' => '',
    'Shop' => '',
    'Tighten Ctrl to select multiple items' => '',
    'Title H1' => '',
    'Vendor code' => '',
    'category: {title}' => '',
    'Акции' => '',
];
