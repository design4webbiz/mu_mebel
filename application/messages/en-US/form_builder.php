<?php
/**
 * Message translations.
 *
 * This file is automatically generated by 'yii message/extract' command.
 * It contains the localizable messages extracted from source code.
 * You may modify this file by translating the extracted messages.
 *
 * Each array element represents the translation (value) of a message (key).
 * If the value is empty, the message is considered as not translated.
 * Messages that no longer need translation will have their translations
 * enclosed between a pair of '@@' marks.
 *
 * Message string can be used with plural forms format. Check i18n section
 * of the guide for details.
 *
 * NOTE: this file must be saved in UTF-8 encoding.
 */
return [
    'Add Field' => '',
    'Add this code to any content to show the form.' => '',
    'Admin Email Content' => '',
    'Are you sure you want to delete this item?' => '',
    'Available variables' => '',
    'Button' => '',
    'CSS and Javascript' => '',
    'Check Box' => '',
    'Common' => '',
    'Create' => '',
    'Create Form Builder Forms' => '',
    'Created' => '',
    'Custom CSS' => '',
    'Custom javascript' => '',
    'Data' => '',
    'Delete' => '',
    'Design' => '',
    'Dropdown List' => '',
    'Edit {fieldName} <span style="color: #f0615c">{fieldLabel}</span>' => '',
    'Email' => '',
    'Email not sending' => '',
    'Email not sending.' => '',
    'Enable Edit' => '',
    'Enable Send To Admin' => '',
    'Enable Send To User' => '',
    'Fields' => '',
    'File Input' => '',
    'Form Builder' => '',
    'Form Builder Forms' => '',
    'Form Builder Submissions' => '',
    'Form ID' => '',
    'Form submited.' => '',
    'Forms' => '',
    'HTML Template' => '',
    'Hidden Input' => '',
    'Html' => '',
    'ID' => '',
    'Ip' => '',
    'Label' => '',
    'Options' => '',
    'Plugin code' => '',
    'Radio List' => '',
    'Redirect to page' => '',
    'Reset' => '',
    'Roles' => '',
    'Save & Continue Edit' => '',
    'Save form before adding design.' => '',
    'Save form before adding email.' => '',
    'Save form before adding fields.' => '',
    'Search' => '',
    'Select a field' => '',
    'Select type' => '',
    'Send' => '',
    'Show Thank you message' => '',
    'Slug' => '',
    'Sort' => '',
    'Status' => '',
    'Submission' => '',
    'Submissions' => '',
    'Text Area' => '',
    'Text Input' => '',
    'Title' => '',
    'Type' => '',
    'Update' => '',
    'Update {modelClass}: ' => '',
    'User Email Content' => '',
    'User Email Field' => '',
    'You can add your CSS declarations here. Do not add &lt;style&gt; tags.' => '',
    'You can add your Javascript declarations here. Do not add &lt;script&gt; tags.' => '',
    'sort' => '',
    '{fieldName} saved.' => '',
    '{n} active submission' => '',
];
