<?php
/**
 * Created by Volodymyr Mazuryk.
 * User: vovan
 * Date: 04.09.17
 * Time: 12:36
 */

namespace app\modules\shop\controllers\frontend;


use app\components\Controller;
use app\modules\shop\models\OrderUserInfos;
use Yii;
use app\modules\shop\models\Order;
use app\modules\shop\models\OrderItems;

class OrderController extends Controller
{
    public function actionOrder()
    {
        $elements = Yii::$app->cart->elements;
        $model = new Order();
        $cart = Yii::$app->cart;

//$dd = Yii::$app->request->post();
//echo '<pre>';print_r($dd['OrderUserInfos']['email']);echo '</pre>';exit;

         if ($model->load(Yii::$app->request->post()) && $model->save() ) {
             $this->addItemsOrder(Yii::$app->request->post(), $elements, $model);
             $this->addUserInfo(Yii::$app->request->post(), $model);
             $this->sendMailTo();

                 foreach ($elements as $ell):
                     $elementModel = $cart->getElementById($ell->id);
                     $elementModel->delete();
                 endforeach;

             return $this->render('order', [
             ]);

         } else {
             return $this->render('order', [
                 'elements' => $elements,
                 'model' => $model,
             ]);
         }
    }

    private function addItemsOrder($post, $elements, &$model)
    {

        foreach ($elements as $element) {
            $orderitems = new OrderItems();
            $modelpr = $element->getModel();

            $orderitems->order_id = $model->id;
            $orderitems->name     = $modelpr->name;
            $orderitems->price    = $element->price;
            $orderitems->count    = $element->count;

            if(!$orderitems->save()) {
                $orderitems->addErrors($orderitems->getErrors());
                return false;
            }
        }

        return true;
    }

    private function addUserInfo($post, &$model)
    {
        $userinfo = new OrderUserInfos();

        $userinfo->order_id   =   $model->id;
        $userinfo->fio	     =   $post['OrderUserInfos']['fio'];
        $userinfo->email	     =   $post['OrderUserInfos']['email'];
        $userinfo->phone	     =   $post['OrderUserInfos']['phone'];
        $userinfo->city	     =   $post['OrderUserInfos']['city'];
        $userinfo->street	 =   $post['OrderUserInfos']['street'];
        $userinfo->home	     =   $post['OrderUserInfos']['home'];
        $userinfo->flat	     =   $post['OrderUserInfos']['flat'];
        $userinfo->floor	     =   $post['OrderUserInfos']['floor'];
        $userinfo->delivery	 =   $post['OrderUserInfos']['delivery'];
        $userinfo->lift	     =   $post['OrderUserInfos']['lift'];
        $userinfo->comment	 =   $post['OrderUserInfos']['comment'];
        $userinfo->dispatch   =   $post['OrderUserInfos']['dispatch'];
        $userinfo->gettofloor =   $post['OrderUserInfos']['gettofloor'];
        $userinfo->sborka     =   $post['OrderUserInfos']['sborka'];
        $userinfo->montag     =   $post['OrderUserInfos']['montag'];

        if(!$userinfo->save()) {
            $userinfo->addErrors($userinfo->getErrors());
            return false;
        }

    }

    private function sendMailTo() {
        $mailer = Yii::$app->mailer
        ->compose()
        ->setFrom(getenv('ROBOT_EMAIL'))
        ->setTo('vovamazuryk@gmail.com')
        ->setSubject('Bye mebel')
        ->setHtmlBody('test');

    }
}