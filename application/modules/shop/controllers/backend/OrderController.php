<?php
/**
 * Created by PhpStorm.
 * User: Vova
 * Date: 29.08.2017
 * Time: 13:58
 */

namespace app\modules\shop\controllers\backend;
use app\components\BackendController;
use app\modules\shop\models\Order;
use app\modules\shop\models\OrderItems;
use Yii;
use yii\data\ActiveDataProvider;

class OrderController extends BackendController
{
    public function actionIndex()
    {
        //
        $searchModel = Order::find()->joinWith('userinfo')->one();
        if(empty(Yii::$app->request->queryParams)){
            $searchModel = new Order();
        }
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,

        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $dataProvider = new ActiveDataProvider([
            'query' => OrderItems::find()->where(['order_id' => $id]),
        ]);

//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['update', 'id' => $model->id]);
//        } else {
            return $this->render('update', [
                'model' => $model,
                'dataProvider' => $dataProvider,
            ]);
       // }
    }

    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested product does not exist.');
        }
    }
}