<?php
use yii\helpers\Html;

?>
<div class="text-index">

    <?php //print_r($searchModel->userinfo->email);// echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="table-responsive">
        <?= \yii\grid\GridView::widget([
            'id' => 'grid',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                [
                    'class' => 'yii\grid\CheckboxColumn',
                    'options' => ['style' => 'width:36px']
                ],
                [
                    'attribute' => 'id',
                    'options' => ['style' => 'width:100px']
                ],
                [
                    'attribute' => 'Order number',
                    'format' => 'raw',
                    'value' => function($model) {
                        return Html::a($model->order_number, ['update', 'id' => $model->id]);
                    }
                ],
                [
                    'attribute' => 'Email',
                    'format' => 'text',
                    'content' => function($model) {
                        return $model->getParentEmail();
                    }
                ],
                [
                    'attribute' => 'Created',
                    'format' => 'raw',
                    'value' => function($model) {
                        return date("d-m-Y H:i:s",$model->created_at);
                    }
                ],
                [
                    'attribute' => 'Summ',
                    'label' => 'Сумма',
                    'format' => 'raw',
                    'value' => function($model) {
                        return $model->order_total.' p.';
                    }
                ],
            ],
        ]); ?>
    </div>
</div>