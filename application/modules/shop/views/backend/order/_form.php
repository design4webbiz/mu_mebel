<?php
/**
 * @package    oakcms
 * @author     Hryvinskyi Volodymyr <script@email.ua>
 * @copyright  Copyright (c) 2015 - 2017. Hryvinskyi Volodymyr
 * @version    0.0.1-alpha.0.4
 */

use app\modules\admin\widgets\Button;
use app\modules\language\models\Language;
use app\modules\shop\models\Category;
use app\modules\shop\models\Producer;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\bootstrap\Html;
use app\modules\admin\widgets\ActiveForm;
use yii\widgets\ListView;

/**
 * @var $this yii\web\View
 * @var $model \app\modules\shop\models\Product
 * @var $form ActiveForm
 * @var $lang Language
 */

yii\jui\JuiAsset::register($this);
$asset = \app\templates\backend\base\assets\BaseAsset::register($this);
\app\modules\shop\assets\BackendAsset::register($this);
echo '<pre>';print_r($model->userinfo);echo '</pre>';
?>

<div class="order-form">

    <?php $form = ActiveForm::begin([
        'options' => [
            'enctype' => 'multipart/form-data',
            'id' => 'product-form-id',
            'data' => [
                'sort-url' => Url::to(['modification/sort'])
            ]
        ]
    ]); ?>

    <?php ActiveForm::end(); ?>

    <?= ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '_list',
    ]);
    ?>
</div>
