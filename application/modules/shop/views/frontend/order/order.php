<?php
/**
 * @package    oakcms
 * @author     Hryvinskyi Volodymyr <script@email.ua>
 * @copyright  Copyright (c) 2015 - 2017. Hryvinskyi Volodymyr
 * @version    0.0.1-beta.0.1
 */

use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use app\modules\filter\models\FilterVariant;
use app\modules\admin\widgets\ActiveForm;

/**
 * @var $this \app\components\CoreView
 * @var $elements \app\modules\cart\events\CartElement[]
 * @var $cart app\modules\cart\Cart
 */
$this->title = Yii::t('cart', 'Cart');
$this->setSeoData($this->title);
$cart = Yii::$app->cart;
$assest = \app\templates\frontend\mu_mebel\assets\BaseAsset::register($this);

$this->params['breadcrumb'][] = "Корзина";

//echo '<pre>';print_r($elements);echo '</pre>';
$modeluserinf = new \app\modules\shop\models\OrderUserInfos();
if (isset($model) && isset($elements)):

?>

<div class="korzina">
    <div class="title text-center">
        <span>Оформление заказа</span>
    </div>

    <div class="row">
        <div class="col-xs-9 col-md-6 col-sm-9">
            <div>Пожалуйста укажите свои контактные данные</div>
            <?php $form = ActiveForm::begin([]); ?>
            <div class="row">

                <div class="col-lg-4 col-xs-4">
                    <div class="form-group">
                        <label for="usr">ФИО*</label>
                        <?= $form->field($modeluserinf,'fio')->textInput()->label(false);?>
                    </div>
                </div>
                <div class="col-lg-4 col-xs-4">
                    <div class="form-group">
                        <label for="usr">Email*</label>
                        <?= $form->field($modeluserinf,'email')->input('email')->label(false);?>
                    </div>
                </div>
                <div class="col-lg-4 col-xs-4">
                    <div class="form-group">
                        <label for="usr">Телефон*</label>
                        <?= $form->field($modeluserinf,'phone')->textInput()->label(false);?>
                    </div>
                </div>
                <div class="col-lg-12 col-xs-12">
                     <div class="form-group">
                        <div class="checkbox">
                            <?= $form->field($modeluserinf, 'dispatch')
                                ->checkbox([
                                    'value' => 1,
                                    'label' => 'Подписаться на рассылку',
                                    'labelOptions' => [
                                        'style' => 'padding-left:20px;'
                                    ]
                                ]); ?>
                        </div>
                     </div>
                </div>
                <div>Пожалуйста укажите свои контактные данные</div>
                <div class="col-lg-3 col-xs-3">
                    <div class="form-group">
                        <label for="usr">Город</label>
                        <?= $form->field($modeluserinf,'city')->textInput()->label(false);?>
                    </div>
                </div>
                <div class="col-lg-3 col-xs-3">
                    <div class="form-group">
                        <label for="usr">Улица</label>
                        <?= $form->field($modeluserinf,'street')->textInput()->label(false);?>
                    </div>
                </div>
                <div class="col-lg-3 col-xs-3">
                    <div class="form-group">
                        <label for="usr">Дом</label>
                        <?= $form->field($modeluserinf,'home')->textInput()->label(false);?>
                    </div>
                </div>
                <div class="col-lg-3 col-xs-3">
                    <div class="form-group">
                        <label for="usr">Квартира</label>
                        <?= $form->field($modeluserinf,'flat')->textInput()->label(false);?>
                    </div>
                </div>
                <div class="col-lg-4 col-xs-4">
                    <div class="form-group">
                        <label for="usr">Этаж</label>
                        <?= $form->field($modeluserinf,'floor')->textInput()->label(false);?>
                    </div>
                </div>
                <div class="col-lg-4 col-xs-4">
                    <div class="form-group">
                        <label for="usr">Желательное время доставки</label>
                        <?= $form->field($modeluserinf,'delivery')->textInput()->label(false);?>
                    </div>
                </div>
                <div class="col-lg-4 col-xs-4">
                    <div class="form-group">
                        <div class="checkbox">
                            <?= $form->field($modeluserinf, 'lift')
                            ->checkbox([
                                'value' => 1,
                                'label' => 'В доме есть лифт',
                                'labelOptions' => [
                                   'style' => 'padding-left:20px;'
                                 ]
                            ]); ?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-xs-12">
                    <div class="form-group">
                        <label for="usr">Коментарий</label>
                        <?= $form->field($modeluserinf,'comment')->textInput()->label(false);?>
                    </div>
                </div>
                <div class="col-lg-12 col-xs-12">
                  <div>Дополнительные услуги</div>
                     <div class="checkbox">
                         <?= $form->field($modeluserinf, 'gettofloor')
                             ->checkbox([
                                 'value' => 1,
                                 'label' => 'Подем на этаж',
                                 'labelOptions' => [
                                     'style' => 'padding-left:20px;'
                                 ]
                             ]); ?>
                     </div>
                     <div class="checkbox">
                         <?= $form->field($modeluserinf, 'sborka')
                             ->checkbox([
                                 'value' => 1,
                                 'label' => 'Сборка мебели: 1000 р.',
                                 'labelOptions' => [
                                     'style' => 'padding-left:20px;'
                                 ]
                             ]); ?>
                     </div>
                     <div class="checkbox">
                         <?= $form->field($modeluserinf, 'montag')
                             ->checkbox([
                                 'value' => 1,
                                 'label' => 'Монтаж техники:стоимось даной услуги уточняйте у менеджера',
                                 'labelOptions' => [
                                     'style' => 'padding-left:20px;'
                                 ]
                             ]); ?>
                     </div>
                </div>
                <?= $form->field($model, 'order_number')->hiddenInput(['value'=> 'N-'.uniqid()])->label(false);  ?>
                <?= $form->field($model, 'order_total')->hiddenInput(['value'=>$cart->getCost()])->label(false); ?>
            </div>
            <div class="col-lg-6 col-xs-6">
                <?= Html::a('Назад в корзину',['/cart/default/index']); ?>
            </div>
            <div class="col-lg-6 col-xs-6">
                <div class="total_prise"><?= Html::submitButton('Оформить заказ'); ?></div>
            </div>
            <?php ActiveForm::end(); ?>

        </div>
        <div class="col-xs-3 col-md-3 col-sm-3">
            <div>Ваш заказ:</div>
        <?php
        foreach ($elements as $element) :
              $modelpro = $element->getModel();
        ?>
            <div class="row">
                <div class="col-xs-6 col-md-9 col-sm-6">
                    <?= $modelpro->name; ?>
                </div>
                <div class="col-xs-3 col-md-3 col-sm-3">
                    <?= $element->price; ?>
                </div>
            </div>

        <?php
        endforeach;
        ?>
            <div class="row">
                <div class="col-xs-12 col-md-12 col-sm-12">
                    Итого: <?= $cart->getCost(); ?>
                </div>
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-xs-3 col-md-2 col-sm-3">
        </div>
    </div>
        <div class="details text-left hidden-sm hidden-xs">
            <img src="<?= $assest->baseUrl ?>/img/details.png" alt="">
            Подробные условия:
        </div>
        <div class="specification row hidden-sm hidden-xs">
            <div class="col-md-3">
                <span>Доставка</span>
                <ul class="list-unstyled">
                    <li><span class="bold_text">Доставка по Москве</span> - безплатно</li>
                    <li><span class="bold_text">Доставка по МО за МКАД</span> - 100p/км</li>
                    <li><span class="bold_text">Доставка по России</span> - до</li>
                    <li>транспортной компании - безплатно</li>
                </ul>
            </div>
            <div class="col-md-3">
                <span>Ожидаемое время доставки</span>
                <ul class="list-unstyled">
                    <li><span class="bold_text">Москва и область</span>- 1-2 дня</li>
                    <li><span class="bold_text">По России</span>- 1-2 дней</li>
                </ul>
            </div>
            <div class="col-md-3">
                <span>Оплата </span>
                <ul class="list-unstyled">
                    <li><span class="bold_text">Москва и МО</span> наличными при</li>
                    <li>получении, безналичными</li>
                    <li><span class="bold_text">Россия</span>- безналичный расчет</li>
                </ul>
            </div>
            <div class="col-md-3">
                <span>Гарантия 12 месяцев</span>
                <ul class="list-unstyled">
                    <li><span class="bold_text">12 месяцев </span> официальной гарантии</li>
                    <li>от производтеля</li>
                    <li>Обмен/возврат товара в течении</li>
                    <li>14 дней, при невскритой упаковке</li>
                </ul>
            </div>
        </div>
</div>
<?php else: ?>

    <div class="container">

        <!-- Modal -->
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Заказ</h4>
                    </div>
                    <div class="modal-body">
                        <p>Спасибо за заказ</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>

    </div>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script>
        $(document).ready(function(){
            $("#myModal").modal();
            $("#myModal").on('hide.bs.modal', function () {
                window.location.href = "/";
            });
        });
    </script>
<?php
    endif;
?>