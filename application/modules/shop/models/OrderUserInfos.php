<?php
/**
 * Created by PhpStorm.
 * User: Vova
 * Date: 29.08.2017
 * Time: 14:09
 */

namespace app\modules\shop\models;


class OrderUserInfos extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%shop_order_userinfos}}';
    }

    public function rules()
    {
        return [
            [['fio','order_id','phone','email'], 'required'],
            ['email', 'email'],
            [['flat','floor','lift','dispatch','gettofloor','sborka','montag'], 'integer'],
            [['city','street','home','delivery'], 'string'],
            ['comment','safe']
        ];
    }

}