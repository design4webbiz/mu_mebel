<?php
/**
 * Created by PhpStorm.
 * User: Vova
 * Date: 29.08.2017
 * Time: 14:06
 */

namespace app\modules\shop\models;

use yii\behaviors\TimestampBehavior;
use yii\data\ActiveDataProvider;


class Order extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%shop_orders}}';
    }

    public function rules()
    {
        return [
            [['order_number', 'order_total'], 'required'],
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function getOrderitems()
    {
        return $this->hasMany(OrderItems::className(), ['order_id' => 'id']);
    }

    public function getUserinfo()
    {
        return $this->hasOne(OrderUserInfos::className(), ['order_id' => 'id']);
    }

    public function getParentEmail()
    {
        $parent = $this->userinfo;

        return $parent ? $parent->email : '';
    }

    public function search($params='')
    {
        $query = Order::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);


        return $dataProvider;
    }
}