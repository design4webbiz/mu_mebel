<?php
/**
 * Created by PhpStorm.
 * User: Vova
 * Date: 29.08.2017
 * Time: 14:09
 */

namespace app\modules\shop\models;


class OrderItems extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return '{{%shop_order_items}}';
    }

    public function rules()
    {
        return [
            [['order_id', 'name', 'price','count'], 'required'],
        ];
    }
}