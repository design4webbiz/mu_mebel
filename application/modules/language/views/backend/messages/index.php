<?php
/**
 * @package    oakcms/oakcms
 * @author     Hryvinskyi Volodymyr <script@email.ua>
 * @copyright  Copyright (c) 2015 - 2017. Hryvinskyi Volodymyr
 * @version    0.0.1-beta.0.1
 */

/**
 * @var $this \app\components\View
 * @var $messages array
 */
use app\assets\AdminLTE;

$idTable = 'messages';

$js = '$(\'#' . $idTable . '\').DataTable({
  "paging": true,
  "lengthChange": true,
  "searching": true,
  "ordering": true,
  "info": true,
  "autoWidth": true
});';

$bundle = AdminLTE::register($this);
$bundle->js[] = 'plugins/datatables/jquery.dataTables.min.js';
$bundle->js[] = 'plugins/datatables/dataTables.bootstrap.min.js';
$bundle->css[] = 'plugins/datatables/dataTables.bootstrap.css';

$this->registerJs($js);

?>



<table id="<?= $idTable ?>" class="table table-bordered table-hover">
    <thead>
    <tr>
        <th>#</th>
        <th><?= Yii::t('language', 'Source') ?></th>
        <th><?= Yii::t('language', 'Message') ?></th>
        <th><?= Yii::t('language', 'Language') ?></th>
        <th><?= Yii::t('language', 'Module') ?></th>
    </tr>
    </thead>
    <tbody>
    <?php $i = 1; ?>
    <?php foreach ($messages as $module => $languages): ?>
        <?php foreach ($languages as $language => $msgs): ?>
            <?php foreach ($msgs as $source => $message): ?>
                <tr>
                    <td><?php echo $i ?></td>
                    <td>
                        <a href="<?= \yii\helpers\Url::to(['update', 'source' => $source, 'module' => $module, 'language' => $language]) ?>">
                            <?php echo $source ?>
                        </a>
                    </td>
                    <td><?php echo $message ?></td>
                    <td><?php echo $language ?></td>
                    <td><?php echo $module ?></td>
                </tr>
                <?php $i++ ?>
            <?php endforeach; ?>
        <?php endforeach; ?>
    <?php endforeach; ?>
    </tbody>
</table>
