<?php
/**
 * @package    oakcms/oakcms
 * @author     Hryvinskyi Volodymyr <script@email.ua>
 * @copyright  Copyright (c) 2015 - 2017. Hryvinskyi Volodymyr
 * @version    0.0.1-beta.0.1
 */

namespace app\modules\language\controllers\backend;

use app\modules\admin\models\Modules;
use Yii;
use app\components\Controller;
use yii\data\ArrayDataProvider;
use yii\helpers\VarDumper;

class MessagesController extends Controller
{

    public function actionIndex() {
        $files = glob(Yii::getAlias('@app/modules/*/messages/*/*.php'));

        $messages = [];

        $modules = Modules::findAllActiveAdmin();

        foreach ($files as $file) {
            $module = basename(dirname(dirname(dirname($file))));

            if(array_key_exists($module, $modules)) {
                $messages[$module][basename(dirname($file))] = require $file;
            }
        }

        return $this->render('index', ['messages' => $messages]);
    }

    public function actionUpdate($source, $module, $language) {
        echo VarDumper::export($language);
    }
}
