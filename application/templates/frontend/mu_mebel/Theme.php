<?php
/**
 * @package    oakcms
 * @author     Hryvinskyi Volodymyr <script@email.ua>
 * @copyright  Copyright (c) 2015 - 2017. Hryvinskyi Volodymyr
 * @version    0.0.1-beta.0.1
 */

namespace app\templates\frontend\mu_mebel;

use app\components\ThemeFrontend;
use Yii;

/**
 * Class Theme
 */
class Theme extends ThemeFrontend
{

    public $themeSourcePath;

    public static $menuItemLayouts = [
        '@frontendTemplate/views/layouts/_clear'  => 'clear',
        '@frontendTemplate/views/layouts/content' => 'content',
        '@frontendTemplate/views/layouts/main'    => 'main'
    ];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->themeSourcePath = '@app/templates/frontend/mu_mebel/web';

        $this->basePath = '@app/templates/frontend/mu_mebel';
        $this->baseUrl = '@web/templates/frontend/mu_mebel/web';

        Yii::$app->assetManager->bundles['yii\bootstrap\BootstrapAsset'] = [
            'sourcePath' => '@app/templates/frontend/mu_mebel/web',
            'css'        => ['css/bootstrap.css']
        ];

        Yii::$app->assetManager->bundles['yii\bootstrap\BootstrapPluginAsset'] = [
            'sourcePath' => '@app/templates/frontend/mu_mebel/web',
            'js'         => ['//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js']
        ];

        Yii::$app->assetManager->bundles['yii\jui\JuiAsset'] = [
            'css' => [
                //'//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.min.css'
            ],
            'js'  => [
                //'//code.jquery.com/ui/1.12.0/jquery-ui.min.js'
            ],
        ];

        Yii::$app->assetManager->bundles['yii\web\JqueryAsset'] = [
            'js' => ['//code.jquery.com/jquery-2.2.4.min.js'],
        ];

        $this->pathMap = [
            '@app/views'   => '@app/templates/frontend/mu_mebel/views',
            '@app/modules' => '@app/templates/frontend/mu_mebel/modules',
            '@app/widgets' => '@app/templates/frontend/mu_mebel/widgets',
        ];
    }
}
