<?php
/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 16.09.2016
 * Project: osnovasite
 * File name: positions.php
 */

return [
    'header'  => 'Header',
    'slider'  => 'Slider',
    'contact' => 'Contact',

    'position_top_1' => 'Position Top 1',
    'position_top_2' => 'Position Top 2',
    'position_top_3' => 'Position Top 3',
    'position_top_4' => 'Position Top 4',
    'position_top_5' => 'Position Top 5',

    'position_bottom_1' => 'Position Bottom 1',
    'position_bottom_2' => 'Position Bottom 2',
    'position_bottom_3' => 'Position Bottom 3',
    'position_bottom_4' => 'Position Bottom 4',
    'position_bottom_5' => 'Position Bottom 5',

    // Footer
    'footer_logo'       => 'Footer logo',
    'footer_slogan'     => 'Footer slogan',
    'footer_social'     => 'Footer social',
    'footer_position_1' => 'Footer position 1',
    'footer_position_2' => 'Footer position 2',
    'footer_position_3' => 'Footer position 3',
    'footer_position_4' => 'Footer position 4',
    'footer_menu_1'     => 'Footer menu 1',
    'footer_menu_2'     => 'Footer menu 2',
    'footer_menu_3'     => 'Footer menu 3',
    'footer_menu_4'     => 'Footer menu 4',
    'footer_menu_5'     => 'Footer menu 5',
    'footer_menu_6'     => 'Footer menu 6',
    'footer_menu_7'     => 'Footer menu 7',
    'footer_menu_8'     => 'Footer menu 8',
];
