var bootstrapButton = $.fn.button.noConflict();
$.fn.bootstrapBtn = bootstrapButton;

(function($) {
    $(document).ready(function () {
        if ($(window).width() <= 1500 && $(window).width() >= 992) {
            var height1 = $(".height_block_1").height();
            $(".height_block_2, .height_block_3").height(height1 - 2);
            $(".height_block_3").css("padding-top", height1 / 20);
        }

        $('.phone_menu').on('click', function () {
            $('.top_menu ul').slideToggle();
        });

        $('.phone_menu_2').on('click', function () {
            $('.menu_2').slideToggle();
            $('.menu_1').slideToggle();
        });

        $('input[type=checkbox], input[type=radio]').not('.simple-switch').uniform();
    });
})(jQuery);
