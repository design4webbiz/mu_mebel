<?php
/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 06.04.2016
 * Project: oakcms
 * File name: _clear.php
 */

/* @var $this \app\components\View */
/* @var $content string */

use app\modules\user\models\LoginForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$bundle = \app\templates\frontend\mu_mebel\assets\BaseAsset::register($this);

$this->bodyClass[] = 'mod_'.Yii::$app->controller->module->id;
$this->bodyClass[] = Yii::$app->controller->id.'_'.Yii::$app->controller->action->id;

$loginForm = new LoginForm();

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<!-- Created by Volodumur Grivinskiy Design4web.biz -->
<html lang="<?= Yii::$app->language ?>">
<head>
    <base href="<?= Yii::$app->homeUrl ?>">
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php
    echo Html::csrfMetaTags();
    $this->registerLinkTag(['rel' => 'canonical', 'href' => Url::canonical()], 'canonical');
    $this->renderMetaTags();
    $this->head();
    ?>

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="<?= implode(' ', (array)$this->bodyClass) ?>">
<?php $this->beginBody() ?>
    <div id="main">
        <?= $content ?>
    </div>

    <?php if(Yii::$app->user->isGuest): ?>
        <div class="modal fade login_modal in" id="myModalLogin" tabindex="-1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img src="<?= $bundle->baseUrl ?>/img/delete.png" alt=""></button>
                        <h4 class="modal-title">Войти</h4>
                    </div>
                    <div class="modal-body">

                        <?php $form = ActiveForm::begin(); ?>
                            <?= $form->field($loginForm, 'username')->textInput() ?>
                            <?= $form->field($loginForm, 'password')->passwordInput() ?>
                            <p class="text-center" style="margin-top: 40px"><a href="">Хочу зарегистрироваться</a></p>
                            <?= $form->field($loginForm, 'rememberMe')->checkbox(); ?>
                            <button type="submit">Войти</button>
                        <?php ActiveForm::end(); ?>

                    </div>
                </div>
            </div>
        </div>
    <?php else: ?>

    <?php endif; ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
