<?php

use app\modules\text\api\Text;

$bundle = \app\templates\frontend\mu_mebel\assets\BaseAsset::register($this);
$this->beginContent('@frontendTemplate/views/layouts/content.php'); ?>

<?= Text::get('header') ?>

<?= Text::get('position_top_1'); ?>
<?= Text::get('position_top_2'); ?>
<?= Text::get('position_top_3'); ?>
<?= Text::get('position_top_4'); ?>
<?= Text::get('position_top_5'); ?>

<div class="container">
    <?php if (isset($this->params['breadcrumbs'])): ?>
        <div class="bread_crums">
            <?= \yii\widgets\Breadcrumbs::widget([
                //'homeLink' => false,
                'tag'      => 'ol',
                'links'    => $this->params['breadcrumbs'],
                'options'  => ['class' => 'breadcrumb'],
            ]); ?>
        </div>
    <?php endif; ?>
    <?= $content ?>
</div>

<?= Text::get('position_bottom_1'); ?>
<?= Text::get('position_bottom_2'); ?>
<?= Text::get('position_bottom_3'); ?>
<?= Text::get('position_bottom_4'); ?>
<?= Text::get('position_bottom_5'); ?>

<div class="row">
    <?= $this->renderFile('@frontendTemplate/views/layouts/blocks/footer.php', ['assets' => $bundle]) ?>
</div>
<div class="hidden">
    <?= $this->renderFile('@frontendTemplate/views/layouts/blocks/svg_icons.php') ?>
</div>
<?php $this->endContent() ?>

