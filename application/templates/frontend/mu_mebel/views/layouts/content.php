<?php
/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 03.07.2015
 * Project: oakcms2
 * File name: content.php
 */

use yii\helpers\ArrayHelper;
use app\modules\text\api\Text;

$this->bodyClass[] = 'base';

?>
<?php $this->beginContent('@frontendTemplate/views/layouts/_clear.php'); ?>

<div class="content">
    <?php if(Yii::$app->session->hasFlash('alert')):?>
        <?= \yii\bootstrap\Alert::widget([
            'body'=>ArrayHelper::getValue(Yii::$app->session->getFlash('alert'), 'body'),
            'options'=>ArrayHelper::getValue(Yii::$app->session->getFlash('alert'), 'options'),
        ])?>
    <?php endif; ?>
    <?= $content ?>
</div>
<?php $this->endContent() ?>
