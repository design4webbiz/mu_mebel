<?php
/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 30.12.2016
 * Project: oakcms
 * File name: svg_icons.php
 */
?>

<svg id="guard-14-day" xmlns="http://www.w3.org/2000/svg" xml:space="preserve" width="81px" height="72px" version="1.1" style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd" viewBox="0 0 624 554">
    <circle class="fil0" cx="314" cy="214" r="214"></circle>
    <circle class="fil1 str0" cx="314" cy="214" r="169"></circle>
    <path class="fil2" d="M576 315l0 -61c0,0 2,-14 10,-17 8,-3 11,-5 16,-5 4,0 12,2 15,8 3,6 7,138 7,138l-80 93 -3 26 -87 0 0 -63 3 -11 85 -88c0,0 10,-4 16,-3 6,2 9,3 12,7 3,3 7,8 7,13l-1 -37zm0 0l1 37 -1 -37z"></path>
    <rect class="fil3" x="434" y="497" width="124.511" height="57.08"></rect>
    <path class="fil4" d="M274 289l-29 0 0 -108c-10,9 -22,17 -36,21l0 -26c7,-2 15,-7 24,-14 9,-6 15,-14 18,-23l23 0 0 150z"></path>
    <path id="1" class="fil4" d="M373 289l0 -30 -61 0 0 -25 64 -95 24 0 0 95 19 0 0 25 -19 0 0 30 -27 0zm0 -55l0 -51 -34 51 34 0z"></path>
    <line class="fil5 str1" x1="572" y1="357" x2="526" y2="409"></line>
    <path class="fil2" d="M48 312l0 -61c0,0 -2,-13 -10,-17 -8,-3 -11,-4 -15,-4 -5,0 -13,1 -16,7 -3,6 -7,138 -7,138l80 94 3 25 87 1 0 -63 -3 -12 -85 -88c0,0 -10,-4 -16,-2 -5,2 -9,3 -12,6 -3,3 -7,8 -6,13l0 -37zm0 0l0 37 0 -37z"></path>
    <rect class="fil3" x="66" y="494" width="124.511" height="57.08"></rect>
    <line class="fil5 str1" x1="52" y1="354" x2="99" y2="406"></line>
</svg>

<svg id="checkbox-ico" xmlns="http://www.w3.org/2000/svg" xml:space="preserve" width="13px" height="13px" version="1.1" shape-rendering="geometricPrecision" text-rendering="geometricPrecision" image-rendering="optimizeQuality" fill-rule="evenodd" clip-rule="evenodd"
     viewBox="0 0 10227 10227"
     xmlns:xlink="http://www.w3.org/1999/xlink">
    <rect fill="#FDFEFE" stroke="#727271" stroke-width="690.995" x="345" y="345" width="9536" height="9536" rx="1162" ry="1162"></rect>
    <polyline fill="none" stroke="#AD0C0C" stroke-width="767.168" stroke-linejoin="round" points="1686,3510 5064,8400 8442,3111 "></polyline>
</svg>
