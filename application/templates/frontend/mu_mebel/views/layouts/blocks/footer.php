<?php
/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 26.08.2016
 * Project: falconcity
 * File name: footer.php
 * @var $assets \app\templates\frontend\base\assets\BaseAsset;
 */

use app\modules\text\api\Text;

$assets = \app\templates\frontend\mu_mebel\assets\BaseAsset::register($this);
?>
<footer>
    <div class="container footer-cntnr">
        <div class="col-lg-6 col-xs-12 no_padding_left no_padding_right">
            <div class="col-md-6 col-sm-6 col-xs-12 no_padding_left no_padding_right">
                <div class="col-xs-6">
                    <?php echo Text::get('footer_menu_1') ?>
                </div>
                <div class="col-xs-6">
                    <?php echo Text::get('footer_menu_2') ?>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 no_padding_left no_padding_right">
                <div class="col-xs-6">
                    <?php echo Text::get('footer_menu_3') ?>
                </div>
                <div class="col-xs-6">
                    <?php echo Text::get('footer_menu_4') ?>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-xs-12 no_padding_left no_padding_right">
            <div class="col-sm-6 col-xs-12 no_padding_left no_padding_right">
                <div class="col-xs-6">
                    <?php echo Text::get('footer_menu_5') ?>
                </div>
                <div class="col-xs-6">
                    <?php echo Text::get('footer_menu_6') ?>
                </div>
            </div>
            <div class="col-sm-6 col-xs-12 no_padding_left no_padding_right">
                <div class="col-xs-6">
                    <?php echo Text::get('footer_menu_7') ?>
                </div>
                <div class="col-xs-6">
                    <?php echo Text::get('footer_menu_8') ?>
                </div>
            </div>
        </div>
    </div>
    <div class="container f_container">

        <div class="both_footer row">
            <div class="col-md-2 col-sm-6 logo">
                <?php echo Text::get('footer_logo') ?>
            </div>
            <div class="col-md-3 col-sm-6 slogan">
                <?php echo Text::get('footer_slogan') ?>
            </div>
            <div class="col-md-7 col-sm-12 list">
                <ul class="list-inline f_list">
                    <li>
                        <?php echo Text::get('footer_position_1') ?>
                    </li>
                    <li>
                        <?php echo Text::get('footer_position_2') ?>
                    </li>
                    <li>
                        <?php echo Text::get('footer_position_3') ?>
                    </li>
                    <li class="text-left">
                        <?php echo Text::get('footer_position_4') ?>
                    </li>
                </ul>

            </div>
            <div class="soc_list col-sm-12 col-md-3">
                <?php echo Text::get('footer_social') ?>
            </div>
        </div>
        <div class="text-center copy">
            <span >2015-<?= date('Y', time()) ?> интернет магазин МЮ мебель</span>
        </div>
    </div>

</footer>
