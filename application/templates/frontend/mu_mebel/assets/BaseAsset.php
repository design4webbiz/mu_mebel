<?php

/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 06.04.2016
 * Project: oakcms
 * File name: BaseAsset.php
 */

namespace app\templates\frontend\mu_mebel\assets;

use yii\web\AssetBundle;
use yii\web\View;

class BaseAsset extends AssetBundle
{
    public $sourcePath = '@app/templates/frontend/mu_mebel/web';
    public $basePath = '@app/templates/frontend/mu_mebel/web';

    public $css = [
        '//fonts.googleapis.com/css?family=Open+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext',
        'css/style.css',
    ];

    public $js = [
        'js/jquery.uniform.min.js',
        'js/common.js'
    ];

    public $depends = [
        'yii\jui\JuiAsset',
        'app\assets\FontAwesome',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];

/*    public $jsOptions = [
        'position' => View::POS_END,
    ];
*/
    public function init()
    {
        $this->publishOptions['forceCopy'] = true;

        parent::init();
    }


    public function __construct($config = [])
    {
        $this->publishOptions['forceCopy'] = true;//(YII_ENV_DEV || YII_ENV_DEV);

        parent::__construct($config);
    }
}
