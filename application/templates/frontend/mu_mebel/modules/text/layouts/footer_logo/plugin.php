<?php
/**
 * @package    oakcms/oakcms
 * @author     Hryvinskyi Volodymyr <script@email.ua>
 * @copyright  Copyright (c) 2015 - 2017. Hryvinskyi Volodymyr
 * @version    0.0.1-beta.0.1
 */

return [
    'name' => 'footer_logo',
    'title' => Yii::t('text', 'Footer logo'),
    'preview_image' => Yii::getAlias('@web').'/application/templates/frontend/mu_mebel/modules/text/layouts/footer_logo/preview.png',
    'viewFile' => '@app/templates/frontend/mu_mebel/modules/text/layouts/footer_logo/view.php',
    'settings' => [
        'logo' => [
            'type' => 'mediaInput',
            'value' => ''
        ],
    ],
];
