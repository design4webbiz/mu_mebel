<?php
/**
 * @package    oakcms/oakcms
 * @author     Hryvinskyi Volodymyr <script@email.ua>
 * @copyright  Copyright (c) 2015 - 2017. Hryvinskyi Volodymyr
 * @version    0.0.1-beta.0.1
 */

use yii\helpers\Html;

/**
 * @var $this \app\components\CoreView;
 * @var $model \app\modules\text\models\Text;
 */

echo Html::img($model->getSetting('logo', ''), []);
