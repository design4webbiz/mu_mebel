<?php
/**
 * @package    oakcms
 * @author     Hryvinskyi Volodymyr <script@email.ua>
 * @copyright  Copyright (c) 2015 - 2017. Hryvinskyi Volodymyr
 * @version    0.0.1-beta.0.1
 */


use yii\helpers\Url;
use yii\helpers\Html;
use app\modules\system\components\Menu;
use app\modules\menu\api\Menu as ApiMenu;
use app\templates\frontend\mu_mebel\assets\BaseAsset;

/**
 * @var $this \app\components\CoreView;
 * @var $model \app\modules\text\models\Text;
 */

$assets = BaseAsset::register($this);

$isHome = (Yii::$app->request->baseUrl.'/index' == Url::to([''])) ? true : false;
?>
<header class="header">
    <div class="container">

        <div class="top_menu">
            <button type="button" class="navbar-toggle phone_menu">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <nav class="clearfix">
                <?= \yii\widgets\Menu::widget([
                    'items'   => ApiMenu::getMenuLvlAll($model->getSetting('menu_top')),
                    'options' => [
                        'class' => 'list-inline pull-left',
                    ],
                ]) ?>
                <ul class="list-inline pull-right">
                    <?php if(Yii::$app->user->isGuest): ?>
                    <li class="login">
                        <a href="" data-toggle="modal" data-target="#myModalLogin">
                            <img src="<?= $assets->baseUrl ?>/img/log-aut.png" alt="">
                            <span>Вход</span>
                        </a>
                    </li>
                        <?php else: ?>
                    <?php endif; ?>
                </ul>
            </nav>
        </div>
        <div class="header__info">
            <div class="row">
                <div class="col-md-6 no_padding">
                    <div class="col-sm-3 col-xs-5 logo-img">
                        <a href="<?= Url::home() ?>">
                            <img class="img-responsive logo" src="<?= $model->getSetting('logo') ?>" alt="МЮ Мебель">
                        </a>
                    </div>
                    <div class="col-sm-5 hidden-xs">
                        <div class="delivery"><?= $model->getSetting('logo_slug') ?></div>
                    </div>
                    <div class="col-sm-4 col-xs-7 time-number-call-you">

                        <div class="row">
                            <div class="work_time">
                                <div><img src="<?= $assets->baseUrl ?>/img/clock.png" alt=""><span
                                            class="work_time_hours"><?= $model->getSetting('working_hours') ?></span></div>

                                <div class="phone_number"><?= $model->getSetting('telephone') ?></div>

                                <div><a href="">Перезвонить Вам?</a></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="guard">
                                <div class="col-xs-12 col-sm-4 hidden-xs hidden-sm icon-guard_cont">
                                    <div class="icon-guard">
                                        <svg class="svg-icon">
                                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#guard-14-day"></use>
                                        </svg>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-8 hidden-xs hidden-sm">
                                    <div class="guarantee"><?= $model->getSetting('guarantee') ?></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 tree-in-one">
                            <ul class="list-inline cart_block">
                                <li>
                                    <a href="javascript:void(0)">
                                        <img src="<?= $assets->baseUrl ?>/img/icon-like.png" alt="">
                                        <p>Нравится</p>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">
                                        <img src="<?= $assets->baseUrl ?>/img/icon-home.png" alt="">
                                        <p>Сравнение</p>
                                    </a>
                                </li>
                                <li>
                                    <?= Html::a(
                                        Html::img($assets->baseUrl . '/img/icon-cart.png') .
                                        Html::tag('p', 'Корзина') .
                                        Html::tag('span', \app\modules\cart\widgets\CartInformer::widget(['text' => '{p}']), ['class' => 'cart__price']),
                                        ['/cart/default/index']
                                    );
                                    ?>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="nav-block">

                <div class="nav-block_menu">
                    <div class="mobile_block no_padding">
                        <span class="hidden-md hidden-lg">Категории товаров</span>
                        <button type="button" class="navbar-toggle phone_menu_2 text-center visible-sm visible-xs">

                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <nav>
                        <?= \yii\widgets\Menu::widget([
                            'items'   => ApiMenu::getMenuLvlAll($model->getSetting('main_menu_1')),
                            'options' => [
                                'class' => 'list-inline menu_1',
                            ],
                        ]) ?>
                        <?= \app\templates\frontend\mu_mebel\widgets\Menu::widget([
                            'items'           => ApiMenu::getMenuLvlAll($model->getSetting('main_menu_2')),
                            'submenuTemplate' => "\n<ul class=\"list-unstyled down_menu_list\">\n{items}\n</ul>\n",
                            'options'         => [
                                'class' => 'list-inline menu_2',
                            ],
                        ]) ?>
                    </nav>
                </div>
            </div>
            <div class="no_padding_left filter">
                <!--<form action="">

                    <div class="dropdown filter_search1 hidden-xs hidden-sm">
                        <a class=" dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">
                            Готовые решения
                            <img src="<?/*= $assets->baseUrl */?>/img/donw-icon.png" alt="">
                        </a>
                        <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                            <li role="presentation"><a role="menuitem" href="#">HTML</a></li>
                            <li role="presentation"><a role="menuitem" href="#">CSS</a></li>
                            <li role="presentation"><a role="menuitem" href="#">JavaScript</a></li>
                            <li role="presentation" class="divider"></li>
                            <li role="presentation"><a role="menuitem" href="#">About Us</a></li>
                        </ul>
                    </div>


                    <div class="dropdown filter_search2 hidden-xs hidden-sm">
                        <a class=" dropdown-toggle " type="button" id="menu2" data-toggle="dropdown">
                            По производителям &nbsp;<img src="<?/*= $assets->baseUrl */?>/img/donw-icon.png" alt=""></a>
                        <ul class="dropdown-menu" role="menu" aria-labelledby="menu2">
                            <li role="presentation"><a role="menuitem" href="#">1424524</a></li>
                            <li role="presentation"><a role="menuitem" href="#">41412452</a></li>
                            <li role="presentation"><a role="menuitem" href="#">52452</a></li>
                            <li role="presentation"><a role="menuitem" href="#">471414</a></li>
                        </ul>
                    </div>
                    <div class="block_search ">
                        <button class="search" type="submit">
                            <img src="<?/*= $assets->baseUrl */?>/img/icon-search.png" alt=""></button>
                        <div class="select">
                            <select class="form-control" name="">
                                <option value="">диваны</option>
                                <option value="">матрасы</option>
                                <option value="">шкафы купе</option>
                            </select>
                        </div>
                    </div>
                </form>-->
            </div>
        </div>
    </div>
</header>
