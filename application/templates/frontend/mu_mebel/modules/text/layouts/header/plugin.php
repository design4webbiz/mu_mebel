<?php
/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 16.09.2016
 * Project: osnovasite
 * File name: plugin.php
 */

return [
    'name' => 'header',
    'title' => Yii::t('text', 'Header'),
    'preview_image' => Yii::getAlias('@web').'/application/templates/frontend/mu_mebel/modules/text/layouts/header/preview.png',
    'viewFile' => '@app/templates/frontend/mu_mebel/modules/text/layouts/header/view.php',
    'settings' => [
        'css_class' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'logo' => [
            'type' => 'mediaInput',
            'value' => ''
        ],
        'logo_slug' => [
            'type' => 'textInput',
            'value' => 'Доставка по Москве и России'
        ],
        'menu_top' => [
            'type' => 'menuType',
            'value' => ''
        ],
        'main_menu_1' => [
            'type' => 'menuType',
            'value' => ''
        ],
        'main_menu_2' => [
            'type' => 'menuType',
            'value' => ''
        ],
        'telephone' => [
            'type' => 'textInput',
            'value' => '8 952 400 8000'
        ],
        'working_hours' => [
            'type' => 'textInput',
            'value' => '9.00-21.00'
        ],
        'guarantee' => [
            'type' => 'textInput',
            'value' => 'Гарантия 14 дней с момента доставки'
        ],
    ],
];
