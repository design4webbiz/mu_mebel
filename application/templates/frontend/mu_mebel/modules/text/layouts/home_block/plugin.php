<?php
/**
 * @package    oakcms
 * @author     Hryvinskyi Volodymyr <script@email.ua>
 * @copyright  Copyright (c) 2015 - 2017. Hryvinskyi Volodymyr
 * @version    v0.0.1-beta.0.1
 */

return [
    'name' => 'home_block',
    'title' => Yii::t('text', 'Home block'),
    'preview_image' => Yii::getAlias('@web').'/application/templates/frontend/mu_mebel/modules/text/layouts/home_block/preview.png',
    'viewFile' => '@app/templates/frontend/mu_mebel/modules/text/layouts/home_block/view.php',
    'settings' => [
        'gallery_widget' => [
            'type' => 'widgetkit',
            'value' => 0
        ],
        'menu' => [
            'type' => 'menuType',
            'value' => ''
        ],
        'ttl1' => [
            'type' => 'textInput',
            'value' => 'везем заказы быстро:'
        ],
        'img1' => [
            'type' => 'mediaInput',
            'value' => 'img/car.png'
        ],
        'descr1' => [
            'type' => 'textarea',
            'value' => 'По Москве 1 день<br>По России 5 дней'
        ],
        'ttl2' => [
            'type' => 'textInput',
            'value' => 'оплату берем по факту:'
        ],
        'img2' => [
            'type' => 'mediaInput',
            'value' => 'img/money.png'
        ],
        'descr2' => [
            'type' => 'textarea',
            'value' => 'Москва по факту доставки<br>Россия по факту отгрузки'
        ],
        'ttl3' => [
            'type' => 'textInput',
            'value' => 'сохраняєм гарантию:'
        ],
        'img3' => [
            'type' => 'mediaInput',
            'value' => 'img/img-icon.png'
        ],
        'descr3' => [
            'type' => 'textarea',
            'value' => 'Гарантия от 1 года<br>Гарантия после сборки'
        ],
        'form' => [
            'type' => 'formBuilder',
            'value' => ''
        ],
    ],
];
