<?php
/**
 * @package    oakcms
 * @author     Hryvinskyi Volodymyr <script@email.ua>
 * @copyright  Copyright (c) 2015 - 2017. Hryvinskyi Volodymyr
 * @version    v0.0.1-beta.0.1
 */


use yii\helpers\Url;
use yii\helpers\Html;
use app\modules\system\components\Menu;
use app\modules\menu\api\Menu as ApiMenu;
use app\templates\frontend\mu_mebel\assets\BaseAsset;

/**
 * @var $this \app\components\CoreView;
 * @var $model \app\modules\text\models\Text;
 */

$assets = BaseAsset::register($this);

$isHome = (Yii::$app->request->baseUrl.'/index' == Url::to([''])) ? true : false;
?>
<div class="container">
    <div class="category height_block_1 hidden-sm hidden-xs">
        <?= \yii\widgets\Menu::widget([
            'items'   => ApiMenu::getMenuLvlAll($model->getSetting('menu')),
            'options' => [
                'class' => 'list-unstyled',
            ],
        ]) ?>
    </div>

    <div class="head_banner height_block_2">
        [widgetkit id='<?= $model->getSetting('gallery_widget') ?>']
    </div>
    <div class="head_block height_block_3 hidden-sm hidden-xs">

        <p class="title"><?= $model->getSetting('ttl1') ?></p>
        <div class="head_block_delivery clearfix">
            <div class="img_block">
                <img class="img-responsive center-block" src="<?= $model->getSetting('img1') ?>" alt="">
            </div>
            <div class="text_block">
                <span><?= $model->getSetting('descr1') ?></span>
            </div>
        </div>

        <p class="title"><?= $model->getSetting('ttl2') ?></p>
        <div class="head_block_cash clearfix">
            <div class="img_block">
                <img class="img-responsive" src="<?= $model->getSetting('img2') ?>" alt="">
            </div>
            <div class="text_block">
                <span><?= $model->getSetting('descr2') ?></span>
            </div>
        </div>

        <p class="title"><?= $model->getSetting('ttl3') ?></p>
        <div class="head_block_varanty clearfix">
            <div class="img_block">
                <img class="img-responsive center-block" src="<?= $model->getSetting('img3') ?>" alt="">
            </div>
            <div class="text_block">
                <span><?= $model->getSetting('descr3') ?></span>
            </div>
        </div>
    </div>
        [form_builder id="<?= $model->getSetting('form') ?>"]
</div>