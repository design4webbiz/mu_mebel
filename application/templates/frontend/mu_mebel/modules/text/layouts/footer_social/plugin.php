<?php
/**
 * @package    oakcms/oakcms
 * @author     Hryvinskyi Volodymyr <script@email.ua>
 * @copyright  Copyright (c) 2015 - 2017. Hryvinskyi Volodymyr
 * @version    0.0.1-beta.0.1
 */

/**
 * Created by Vladimir Hryvinskyy.
 * Site: http://codice.in.ua/
 * Date: 16.09.2016
 * Project: osnovasite
 * File name: plugin.php
 */

return [
    'name' => 'footer_social',
    'title' => Yii::t('text', 'Footer Social'),
    'preview_image' => Yii::getAlias('@web').'/application/templates/frontend/mu_mebel/modules/text/layouts/footer_social/preview.png',
    'viewFile' => '@app/templates/frontend/mu_mebel/modules/text/layouts/footer_social/view.php',
    'settings' => [
        'title' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'vk_link' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'ok_link' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'facebook_link' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'twitter_link' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'google_plus_link' => [
            'type' => 'textInput',
            'value' => ''
        ],
        'moy_mir_link' => [
            'type' => 'textInput',
            'value' => ''
        ],
    ],
];
