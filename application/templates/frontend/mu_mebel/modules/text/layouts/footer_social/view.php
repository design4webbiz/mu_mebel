<?php
/**
 * @package    oakcms/oakcms
 * @author     Hryvinskyi Volodymyr <script@email.ua>
 * @copyright  Copyright (c) 2015 - 2017. Hryvinskyi Volodymyr
 * @version    0.0.1-beta.0.1
 */

use app\templates\frontend\mu_mebel\assets\BaseAsset;

/**
 * @var $this \app\components\CoreView;
 * @var $model \app\modules\text\models\Text;
 */
$assets = BaseAsset::register($this);
?>
<p><?= $model->getSetting('title', 'Присоидиняйтесь к нам:') ?></p>
<ul class="list-inline list-unstyled">
    <li>
        <a href="<?= $model->getSetting('vk_link', '#') ?>">
            <img src="<?= $assets->baseUrl ?>/img/f_vk.png" alt="vk">
        </a>
    </li>
    <li>
        <a href="<?= $model->getSetting('ok_link', '#') ?>">
            <img src="<?= $assets->baseUrl ?>/img/f_ok.png" alt="ok">
        </a>
    </li>
    <li>
        <a href="<?= $model->getSetting('facebook_link', '#') ?>">
            <img src="<?= $assets->baseUrl ?>/img/f_fb.png" alt="fb">
        </a>
    </li>
    <li>
        <a href="<?= $model->getSetting('twitter_link', '#') ?>">
            <img src="<?= $assets->baseUrl ?>/img/f_tw.png" alt="fb">
        </a>
    </li>
    <li>
        <a href="<?= $model->getSetting('google_plus_link', '#') ?>">
            <img src="<?= $assets->baseUrl ?>/img/f_gl.png" alt="google">
        </a>
    </li>
    <li>
        <a href="<?= $model->getSetting('moy_mir_link', '#') ?>">
            <img src="<?= $assets->baseUrl ?>/img/f_sm.png" alt="sm">
        </a>
    </li>
</ul>
