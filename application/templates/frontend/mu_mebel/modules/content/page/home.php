<?php
/**
 * Created by PhpStorm.
 * User: michael
 * Date: 23.10.17
 * Time: 11:03
 *
 * @var $model \app\modules\content\models\ContentPages;
 */


$this->bodyClass = ['page-'.$model->id];

$this->setSeoData($model->title, $model->description, '');
?>
